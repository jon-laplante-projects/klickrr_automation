namespace :mail_tasks do
  desc ""
  task :message_sender => :environment do
    # Find broadcasts and followups that are due for sending.
    if Broadcast.where("sent IS NULL AND utc_push_at < '#{Time.now.utc}'").count > 0
      AutomationWorker.new.perform_async("broadcast")
    end

    # fu = Followup.where("")
  end
end
