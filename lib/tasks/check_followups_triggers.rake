desc "This task checks perfoming of followups triggers"
task check_followups_triggers: :environment do
  # No action on sent email in {{x}} days.
  Followup.where(trigger_id: 1, active: true).find_each do |followup|
    sent_followups = SentEmailFollowup.where(followup_id: followup.id).pluck(:sent_mail_id)
    variables = followup.trigger_variables.blank? ? nil : eval(followup.trigger_variables)
    unless variables.blank?
      unless sent_followups.blank?
        sent_during_range = followup.sent_mails.where(["message_type = 'broadcast' AND message_id = ? AND clicks IS NULL AND opened IS NULL AND dropped IS NULL AND bounced IS NULL AND unsubscribed IS NULL AND spam IS NULL AND blocked IS NULL AND sent_mails.created_at < ? AND sent_mails.email NOT IN (SELECT queued_messages.addr FROM queued_messages WHERE followup_id = ?) AND sent_mails.id NOT IN (?)", followup.message_id, DateTime.now - variables["x"].to_i.days, followup.id, sent_followups])
      else
        sent_during_range = followup.sent_mails.where(["message_type = 'broadcast' AND message_id = ? AND clicks IS NULL AND opened IS NULL AND dropped IS NULL AND bounced IS NULL AND unsubscribed IS NULL AND spam IS NULL AND blocked IS NULL AND sent_mails.created_at < ? AND sent_mails.email NOT IN (SELECT queued_messages.addr FROM queued_messages WHERE followup_id = ?)", followup.message_id, DateTime.now - variables["x"].to_i.days, followup.id])
      end
    end
    if sent_during_range.present?
      sent_during_range.each do |sent|
        followup.followup_message_parts.each do |msg|
          queued_message = QueuedMessage.new
          queued_message.addr = sent.email
          queued_message.followup_id = followup.id
          queued_message.subject = msg.subject
          queued_message.message = msg.message
          queued_message.send_on = Time.now
          queued_message.save

          sent_email_followups = SentEmailFollowup.new
          sent_email_followups.sent_mail_id = sent.id
          sent_email_followups.followup_id = followup.id
          sent_email_followups.sent_at = DateTime.now
          sent_email_followups.save
        end
      end
    end
  end

  # {{x}} days since broadcast was sent.
  Followup.where(trigger_id: 10, active: true).find_each do |followup|
    sent_followups = SentEmailFollowup.where(followup_id: followup.id).pluck(:sent_mail_id)
    variables = eval(followup.trigger_variables)
    unless sent_followups.blank?
      sent_during_range = followup.sent_mails.where(["message_type = 'broadcast' AND message_id = ? AND dropped IS NULL AND bounced IS NULL AND unsubscribed IS NULL AND spam IS NULL AND blocked IS NULL AND sent_mails.created_at::DATE = ?::DATE AND sent_mails.email NOT IN (SELECT queued_messages.addr FROM queued_messages WHERE followup_id = ?) AND sent_mails.id NOT IN (?)", followup.message_id, DateTime.now - variables["x"].to_i.days, followup.id, sent_followups])
    else
      sent_during_range = followup.sent_mails.where(["message_type = 'broadcast' AND message_id = ? AND dropped IS NULL AND bounced IS NULL AND unsubscribed IS NULL AND spam IS NULL AND blocked IS NULL AND sent_mails.created_at::DATE = ?::DATE AND sent_mails.email NOT IN (SELECT queued_messages.addr FROM queued_messages WHERE followup_id = ?)", followup.message_id, DateTime.now - variables["x"].to_i.days, followup.id])
    end
    if sent_during_range.present?
      sent_during_range.each do |sent|
        followup.followup_message_parts.each do |msg|
          queued_message = QueuedMessage.new
          queued_message.addr = sent.email
          queued_message.followup_id = followup.id
          queued_message.subject = msg.subject
          queued_message.message = msg.message
          queued_message.send_on = Time.now
          queued_message.save

          sent_email_followups = SentEmailFollowup.new
          sent_email_followups.sent_mail_id = sent.id
          sent_email_followups.followup_id = followup.id
          sent_email_followups.sent_at = DateTime.now
          sent_email_followups.save
        end
      end
    end
  end
end