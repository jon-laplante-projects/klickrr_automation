namespace :followup_time_events do
  desc ""
  task :poll_for_followups => :environment do
    # Find broadcasts and followups that are due for sending.
    if FollowUp.where(["trigger_id IN (?) AND active IS true AND ", [1, 10]]).count > 0
    # if FollowUp.where(trigger_id: 1, active: true).count > 0
      AutomationWorker.new.perform_async("broadcast")
    end
  end
end
