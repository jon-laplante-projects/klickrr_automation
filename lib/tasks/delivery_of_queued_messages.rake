desc "This task checks new messages for delivary"
task delivery_of_queued_messages: :environment do
  QueuedMessage.where(["delivery_on IS NULL AND send_on::DATE <= ?::DATE", Time.now]).each do |msg|
    # Initial data grab.
    followup = Followup.find(msg.followup_id)
    list_id = followup.list_id
    list = List.find(list_id)
    if list.services.blank?
      Rails.logger.info "Can not send queued message with id = #{msg.id}. No active services in list."
      next
    end

    user_id = followup.user_id

    active_services = List.select(:services).find(followup.list_id)
    list_field_group_ids = ListFieldGroup.where(["list_id = ?", list_id]).pluck(:id)
    set_id = ListValue.where(["list_field_group_id IN (?) AND field_value = ?", list_field_group_ids, msg.addr]).pluck(:set_id)

    config = {}
    config["user_id"] = user_id
    config["subject"] = msg.subject
    config["to_email"] = msg.addr
    config["from_email"] = "" # list.from_email
    config["domain"] = list.domain
    #list = msg.addr.to_json

    # Get the user object.
    usr = User.find(user_id)

    # Services
    if list.services.blank?
    #if active_services.blank?
      #services = ServicesUsers.find_by_sql(["SELECT services.id, name, usage, active, service_id FROM services_users LEFT OUTER JOIN services ON (services.id = service_id) WHERE active = ? AND user_id = ?", true, usr.id])
      services = ServicesUser.find_by_sql(["SELECT services.id, name, usage, active, service_id, service_type_id, used FROM services_users LEFT OUTER JOIN services ON (services.id = service_id) WHERE user_id = ? AND used = ?", usr.id, true])
    else
      active_services_arr = active_services.services.split(",")
      #services = ServicesUsers.find_by_sql(["SELECT services.id, name, usage, active, service_id FROM services_users LEFT OUTER JOIN services ON (services.id = service_id) WHERE active = ? AND user_id = ? AND services.id IN(?)", true, user_id, active_services_arr])
      services = ServicesUser.find_by_sql(["SELECT services.id, name, usage, active, service_id, service_type_id, used FROM services_users LEFT OUTER JOIN services ON (services.id = service_id) WHERE user_id = ? AND services.id IN(#{ list.services }) AND used = ?", usr.id, true])
    end

    service_index = rand(0..services.count - 1)
puts "service_id: #{service_index}"
    # Get the user's service preferences (i.e. which is active, logins, etc.)
    service_user = ServicesUser.find_by(service_id: services[service_index].id, user_id: list.user_id)
    usage = JSON.parse(service_user.usage)

    usage.each do |key, param|
      config[key] = param
    end

    # Save this 'sent' to the sent_mail table.
    sent_mail = SentMail.new
    sent_mail.user_id = config["user_id"]
    sent_mail.service_id = services[service_index].id
    sent_mail.message_id = followup.id
    # sent_mail.contact_id = contact.id
    sent_mail.message_type = "followup"
    sent_mail.email = msg.addr
    sent_mail.generate_token("tracking_code", 12)
    sent_mail.dirty = true
    sent_mail.save

    config["sent_mail_id"] = sent_mail.id

    sent_mail = SentMail.find(config["sent_mail_id"])

    # Instantiate MailTasks.
    mail_tasks = MailTasks.new

    # Let's get our message built!
    service_desc = Service.find(services[service_index].service_id)

    #if  service_desc.is_smtp? ||  service_desc.name == "ses" ||  service_desc.name == "postage"
      footer = "<hr style=\"margin: 15px 0 15px 0;\"/><div style=\"text-align: center; font-size: 0.75em\"><a href=\"http://klickrr.net/unsubscribe/me/#{sent_mail.tracking_code}\" target=\"_BLANK\">Please remove me from this list.</a>&nbsp;&bull;&nbsp;Provided by <a target=\"_BLANK\" href=\"klickrr.net\">Klickrr.net</a>.</div>"
    #else
    #  footer = "<div style=\"text-align: center; font-size: 0.75em\">Provided by <a target=\"_BLANK\" href=\"klickrr.net\">Klickrr.net</a>.</div>"
    #end

    # if  service_desc.is_smtp? ||  service_desc.name == "ses" ||  service_desc.name == "postage"
      content = mail_tasks.replace_links_in_message(msg.message, sent_mail.tracking_code)
    # end

    config["service"] = "#{ service_desc.name}"

    # Prep our email for delivery.
    message = <<-CONTENT
									<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
									<html xmlns="http://www.w3.org/1999/xhtml">
									<head>
									<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
									<title>Message for #{msg.addr}</title>
									<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
									</head>
									<body>
									<img src="http://www.klickrr.net/open.png?tc=#{ sent_mail.tracking_code }"#{ "mc:disable-tracking" if services[service_index].name == "mandrill" } />
									#{content}
    #{footer}
									</body>
									</html>
    CONTENT

    config["content"] = message

    # Let's get that message sent!
    config["service"] = "#{ service_desc.name}"

    result = service_desc.is_smtp? ? mail_tasks.send_smtp(config) : mail_tasks.send_api(config)

    msg.delivery_on = Time.now
    msg.save
  end
end
