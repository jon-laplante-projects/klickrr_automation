class AwsSqs
  include Singleton

  def initialize
      Aws.config.update({
        region: 'us-east-1',
        credentials: Aws::Credentials.new('AKIAITGI3U27U5NOXWLQ', 'twXtjYsRNGc4aXDq+rtGIHr3YrpN++ZIRImTFAwo')
        # credentials: Aws::Credentials.new(ENV['AWS_ACCESS_KEY_ID'], ENV['AWS_SECRET_ACCESS_KEY'])
      })
      @client = Aws::SQS::Client.new
    end

    def get_or_create_queue
      queue = @client.create_queue(queue_name: "location_lookup", 
                   attributes: { DelaySeconds: "0", MessageRetentionPeriod: "3600" })
      queue[:queue_url]
    end

    def send_message(email_id, request_ip)
      message_body = { email_id: email_id, request_ip: request_ip }
      @client.send_message(queue_url: get_or_create_queue, message_body: message_body.to_json)
    end

end
