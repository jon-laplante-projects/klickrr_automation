require 'net/http'
require 'action_view'
require 'SmtpApiHeader.rb'
require "resolv-replace.rb"
require 'valid_email'

module OldMailSender extend self
  include SendingApi

  def message_sender(id_val, send_type_val, args)
    #################################################
  ## TODO: Move this, send_smtp and send_api into
  ## their own class and simply call them from
  ## MailController.
  #################################################
    # Thread.new do
      config = {}
      # Determine what time of mailing this is, and pull data as is appropriate.
      case send_type_val
        when "Broadcast"
          bc = Broadcast.find(id_val)
          bc.update(state: :processing)
          config["subject"] = bc.subject.clone
          master_subject = bc.subject.clone
          master_content = bc.content

          list = bc.list

          segments = []
          if bc.segments.count > 0
            bc.segments.each { |segment| segments = [segments, segment.get_emails].reject(&:empty?).reduce(:&)}
          end

        when "followup"
          fu = Followup.find(id_val)
          config["subject"] = fu.subject
          master_subject = fu.subject
          master_content = fu.content
          list = fu.broadcasts.list
          segments = nil
          # segments = get_segments(fu.list.user_id, fu.followup_segments, list)

        when "conn_test"
          config["subject"] = "Klickrr :: Testing #{ config["service"] } Connection"
          master_subject = "Klickrr :: Testing #{ config["service"] } Connection"
          master_content = "This is a test of the #{ config["service"] } service in Klickrr."
            service = args["service_id"]
          list = {"email" => args["email"]}.to_json

        when "email_preview"
          # Put together the email preview var inits.
          config["subject"] = "#{args["subject"]}"
          master_subject = "#{args["subject"]}"
          master_content = "#{args["content"]}"
          list = {"email" => args["email"]}.to_json

        when "test_message_from_broadcast"
          config["subject"] = "#{args["subject"]}"
          master_subject = "#{args["subject"]}"
          config["email"] = "#{args['email']}"
          master_content = "#{args["content"]}"
          # fake list
          list = List.new(id: args["list_id"], user_id: User.first.id)
          fake_contact = Struct.new(:id, :email, :set_id, :attributes, :first_name)
          contact = fake_contact.new(0, args['email'], 0, {}, 'test_name')
          fake_contacts = [contact]
      end

      #unless args["user_id"].blank?
      config["user_id"] = args[:user_id]
      #end
      # Set our content variable - we won't touch master_content.
      content = master_content
      # Because this is using the old 'data bag' model, we need to
      # get the column name/label order from list_values and then
      # match them up the the labels in list_fields.
      list_groups = ListField.find_by_sql(["SELECT DISTINCT list_field_group_id FROM list_fields WHERE list_id = ? ORDER BY list_field_group_id ASC;", list.id])
      list_groups.each do |group|
        columns = ListField.find_by_sql(["SELECT label FROM list_fields LEFT OUTER JOIN list_values ON (list_values.list_field_id = list_fields.id) WHERE list_values.list_field_group_id = ? ORDER BY list_values.id", group.list_field_group_id])
        #columns = ListField.find_by_sql(["SELECT label FROM list_fields LEFT OUTER JOIN list_values ON (list_values.list_field_id = list_fields.id) WHERE list_id = ? ORDER BY list_values.id", list.id])
          cols = Array.new

          columns.each do |c|
            cols << c.label unless cols.include? c.label
          end

        col_string = "set_id int"
        cols.each do |col|
          col_string.concat(", #{col} text")
        end


        #################################################
        ## TODO: Figure out a way to get all columns, while
        ## honoring the correct order, and still
        ## identifying email addresses.
        #################################################

        # #contacts = ListValue.find_by_sql(["SELECT * from crosstab('SELECT set_id, label, field_value FROM list_fields LEFT OUTER JOIN list_values ON (list_values.list_field_id = list_fields.id) WHERE list_id = ? ORDER BY list_values.id') AS ct(set_id int, first_name text, last_name text, email text, phone text)", list.id])
        # contacts = ListValue.find_by_sql(["SELECT * from crosstab('SELECT set_id, label, field_value FROM list_fields LEFT OUTER JOIN list_values ON (list_values.list_field_id = list_fields.id) WHERE list_values.list_field_group_id = ? ORDER BY list_values.id') AS ct(#{col_string})", group.list_field_group_id])
        # #puts "#{contacts.to_json}"
        # #contacts = ListValue.find_by_sql("SELECT * from crosstab('SELECT set_id, label, field_value FROM list_fields LEFT OUTER JOIN list_values ON (list_values.list_field_id = list_fields.id) WHERE list_id = #{params[:list_id].to_i} AND set_id IN (#{record_sets.join(",")}) ORDER BY list_values.id') AS ct(#{col_string})")

        usr = list.user
        if list.services.blank?
          services = ServicesUser.find_by_sql(["SELECT services.id, name, usage, active, service_id, service_type_id, used FROM services_users LEFT OUTER JOIN services ON (services.id = service_id) WHERE user_id = ? AND used = ?", usr.id, true])
          #services = ServicesUser.find_by_sql(["SELECT services.id, name, usage, active, service_id, service_type_id FROM services_users LEFT OUTER JOIN services ON (services.id = service_id) WHERE active = ? AND user_id = ?", true, usr.id])
        else
          services = ServicesUser.find_by_sql(["SELECT services.id, name, usage, active, service_id, service_type_id, used FROM services_users LEFT OUTER JOIN services ON (services.id = service_id) WHERE user_id = ? AND services.id IN(#{ list.services }) AND used = ?", usr.id, true])
          # services = ServicesUser.find_by_sql(["SELECT services.id, name, usage, active, service_id, service_type_id FROM services_users LEFT OUTER JOIN services ON (services.id = service_id) WHERE active = ? AND user_id = ? AND services.id IN(#{ list.services })", true, usr.id])
        end

        # Set up services to rotate through each that is active.
        service_index = 0

        api_key = "api_key"
        api_name = "api_name"

        json_arr = Array.new

        columns = ListField.find_by_sql(["SELECT '{{' || label || '}}' AS token, label, id, list_id, list_field_group_id FROM list_fields WHERE list_id = ? ORDER BY list_fields.id;", list.id])

        # We're going to pull batches of 1,000 rows to process.
        offset = 0
        limit = 1000
        contacts = fake_contacts if fake_contacts.present?
        contacts ||= ListValue.find_by_sql(["SELECT * from crosstab('SELECT set_id, label, field_value FROM list_fields LEFT OUTER JOIN list_values ON (list_values.list_field_id = list_fields.id) WHERE list_values.list_field_group_id = ? ORDER BY list_values.id') AS ct(#{col_string}) LIMIT 1000", group.list_field_group_id])
        # As long as we've got contacts, keep batching in 1,000s.
        while contacts.present?
          # Unless there are segments *AND* the current email is not in the segmented list.while contacts.present?
          offset += limit
          contacts.each do |contact|
            # Unless there are segments *AND* the current email is not in the segmented list.
            if segments.blank? || segments.include?(contact.email)
              # Let's check and see if there are unsubscribers in this list.
              unsubscribed = Unsubscribe.where(set_id: contact.set_id).size
              unless unsubscribed > 0
                config.merge!(contact.attributes)
                # Check and make sure that this email address hasn't already received  this message.
                sent_mail = SentMail.where(["message_id = ? AND message_type = ? AND message_type != 'service_test' AND email = ?", id_val, send_type_val, contact.email]).count
                unless sent_mail > 0

                  # Get the user's service preferences (i.e. which is active, logins, etc.)
                  service_user = ServicesUser.find_by(service_id: services[service_index].id, user_id: list.user_id, used: true)
                  config.merge!(JSON.parse(service_user.usage))

                  # List does not track domain or sender, so pull it from the usage JSON.
                  usage = JSON.parse(service_user.usage)
                  config["domain"] = usage["domain"] unless usage["domain"].blank?
                  config["sender"] = usage["sender"] unless usage["sender"].blank?

                  config["sendmail"] = true


                  # Update the message content - replace {{placeholders}}, add footer, and tracking pixel.
                  #tracking_code = SecureRandom.hex(9)
                  ###################################
                  ## TODO: Ensure the tracking_code
                  ## is unique.
                  ###################################
                  # Save this 'sent' to the sent_mail table.
                  sent_mail = SentMail.new
                    sent_mail.user_id = config["user_id"]
                    sent_mail.service_id = services[service_index].id
                    sent_mail.message_id = id_val
                    sent_mail.contact_id = contact.id
                    sent_mail.message_type = send_type_val
                    sent_mail.email = contact.email
                    sent_mail.sent = false
                    sent_mail.generate_token("tracking_code", 12)
                  sent_mail.save

                  if args[:test_message_b]
                    if Random.rand(2) == 1
                      sent_mail.update_attribute(:message_id, args[:test_message_b].id)
                      config["subject"] = args[:test_message_b].subject
                      content = args[:test_message_b].content
                    end
                  end
                  config["sent_mail_id"] = sent_mail.id
                  config["to_email"] = config.delete("email")


                  columns.each do |column|
                    unless contact[column[:label].parameterize.underscore.to_sym].blank?
                      config["subject"] = config["subject"].to_s.gsub(column[:token], contact[column[:label].parameterize.underscore.to_sym])
                      content = content.to_s.gsub(column[:token],contact[column[:label].parameterize.underscore.to_sym])
                    else
                      config["subject"] = config["subject"].gsub(column[:token], "")
                      content = content.to_s.gsub(column[:token],"")
                    end
                  end


                  # Let's get our message built!
                  service_desc = services[service_index].service

                  #puts "Service: #{services_desc.name}"

                  # if service_desc.is_smtp? || service_desc.name == "ses" || service_desc.name == "postage" || service_desc.name == "sendgrid"
                    footer = "<hr style=\"margin: 15px 0 15px 0;\"/><div style=\"text-align: center; font-size: 0.75em\"><a href=\"http://klickrr.net/unsubscribe/me/#{sent_mail.tracking_code}\" target=\"_BLANK\">Please remove me from this list.</a>&nbsp;&bull;&nbsp;Provided by <a target=\"_BLANK\" href=\"klickrr.net\">Klickrr.net</a>.</div>"
                    content = replace_links_in_message(content, sent_mail.tracking_code)

                  # else
                  #   footer = "<div style=\"text-align: center; font-size: 0.75em\">Provided by <a target=\"_BLANK\" href=\"klickrr.net\">Klickrr.net</a>.</div>"
                  # end

                  #######################################################
                  ## TODO: Unsubscribe endpoint can stay the same, but the
                  ## link will eventually be conditionally and only for
                  ## services that don't provide there own.
                  ##
                  ##
                  ## TODO: Add conditionals for services with webhooks.
                  ##
                  ## Don't forget to handle the tracking pixel
                  ## appropriately, too.
                  #######################################################
                  #content = replace_links_in_message(content)

                  # Prep our email for delivery.


                  #message = message.concat('<hr style="margin: 15px 0 15px 0;"/><div style="text-align: center;">Provided by <a href="klickrr.net">Klickrr.net</a></div>')
                  #message = message.concat("</body></html>")

                  # Check to see if we are sending something other than email.
                  service_type = services[service_index].service.service_type


                  if ["email", "smtp_email"].include? service_type.name
                    config["content"]  = <<-CONTENT
                    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
					          <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                    </head>
                    <body>
                    <img src="http://www.klickrr.net/open.png?tc=#{ sent_mail.tracking_code }" #{ "mc:disable-tracking" if services[service_index].name == "mandrill" } />
                    #{content}
                    #{footer}
                    </body>
                    </html>
                    CONTENT
                  else
                    config["content"] = content
                  end

                  # Let's get that message sent!
                  #services_desc = Service.find(services[service_index].service_id)

                  config["service"] = Service.find(services[service_index][:service_id]).name
                  result = service_desc.is_smtp? ? SendingApi.send_smtp(config) : SendingApi.send_api(config)

                  if result
                    @sent_count ||= 0
                    @sent_count += 1
                    bc.update(sent_count: @sent_count) if bc
                  end

                  # Reset content so it's not sending the same message again and again.
                  content = master_content
                  config["subject"] = master_subject

                  # Rotate service used.
                  if service_index == services.count - 1
                    service_index = 0
                  else
                    service_index += 1
                  end

                  # Don't forget to move SMTP stuff to the API OR call it seperately.
                else
                  sent_mail = SentMail.new
                    sent_mail.user_id = config["user_id"]
                    sent_mail.service_id = services[service_index].id
                    sent_mail.message_id = id_val
                    sent_mail.contact_id = contact.id
                    sent_mail.message_type = "broadcast"
                    sent_mail.email = contact.email
                    sent_mail.sent = false
                    sent_mail.response_message = "This contact has already been sent this message."
                  sent_mail.save
                end # Closing UNLESS this message has been sent to this contact...
              end
            end
          end
          # Get the next batch of 1,000 contacts.
          contacts = ListValue.find_by_sql(["SELECT * from crosstab('SELECT set_id, label, field_value FROM list_fields LEFT OUTER JOIN list_values ON (list_values.list_field_id = list_fields.id) WHERE list_values.list_field_group_id = ? ORDER BY list_values.id') AS ct(#{col_string}) OFFSET #{offset} LIMIT #{limit}", group.list_field_group_id])
        end
      end

      bc.update(sent: Time.now, state: :completed) if bc.present?
      args[:test_message_b].update(sent: Time.now, state: :completed) if args[:test_message_b].present?
      fu.update_attribute(:sent, Time.now) if fu.present?
      # end # Close the thread
  end

  def replace_links_in_message(msg, tracking_code)
    message = Nokogiri::HTML.fragment(msg)
    if tracking_code.include? "=3D"
      tracking_code == tracking_code.unpack('M')[0]
    end

    message.css('a[href]').each do |link|
    #################################################
    ## TODO: Change to SSL when implemented.
    #################################################
      link["href"] = "http://www.klickrr.net/lnks/#{tracking_code}?url=#{link['href']}"
    end

    return message.to_html
  end
end
