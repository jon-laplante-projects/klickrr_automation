class MailTasks
 require "sending_api.rb"
 require "mail_sender.rb"
 include SendingApi

  def initialize()

  end

  ##############################################################
  ##############################################################
  def test_whenever
    # File("test.txt", "w") do |f|
    #   f.write("Another test at #{Time.now}.")
    #  end
    puts "Test Wheneverize!"
  end
  ##############################################################
  ##############################################################

  def find_scheduled_broadcasts
    Broadcast.where("sent IS NULL AND utc_push_at < '#{Time.now.utc}'")
  end

  # sent is null
  # push_at passed.

  def message_sender(id_val, send_type_val, args)
    MailSender.old_calling(id_val, send_type_val, args)
    # OldMailSender.message_sender(id_val, send_type_val, args) # old api
  end
end
