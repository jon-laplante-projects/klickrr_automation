class MailSender::BroadcastTestMailSender < MailSender::Base
  def initialize(args)
    list    = List.find args['list_id']
    user_id = args['user_id']
    content = args['content']
    subject = args['subject']
    @email  = args['email']

    @provider         = MailSender::ServiceInterfaceProvider.new user_id: user_id, list: list

    sent_mail_helper  = MailSender::SentMailHelper.new message_type: 'broadcast', user_id: user_id
    @builder          = MailSender::MailBuilder.new(sent_mail_helper: sent_mail_helper,
                                                    content:          content,
                                                    subject:          subject,
                                                    list:             list)
  end

  def perform
    interface = @provider.get_interface
    mail = @builder.build_mail fake_contact, interface

    interface.send mail
  end

  private

  def fake_contact
    Struct.new(:id, :email, :set_id, :attributes)
          .new(0,   @email, 0,       {})
  end
end
