module MailSender::ContactFilters
  def in_segment?(contact)
    in_segment = @segments.blank? || @segments.include?(contact.email)
  end

  def unsubscribed?(contact)
    Unsubscribe.where(set_id: contact.set_id).size > 0
  end

  def get_unsubscribed_set_ids(set_ids)
    Unsubscribe.where(set_id: set_ids).pluck(:set_id)
  end

  def mail_already_sent?(contact)
    sent_mail = SentMail.where(["message_id = ? AND message_type = ? AND message_type != 'service_test' AND email = ?",
      @message_id, @message_type, contact.email]).count
    sent_mail > 0
  end

  def get_mails_already_sent(contacts)
    SentMail.where(["message_id = ? AND message_type = ? AND message_type != 'service_test' AND email in (?)",
       @message_id, @message_type, contacts]).pluck(:email)
  end
end
