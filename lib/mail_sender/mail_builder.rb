class MailSender::MailBuilder
  def initialize(args)
    @list             = args[:list]
    @subject          = args[:subject]
    @content          = args[:content]
    @sent_mail_helper = args[:sent_mail_helper]

    get_columns
  end

  def build_mail(contact, service_interface)
    filled_content = get_content_for contact
    sent_mail = create_initial_sent_mail contact, service_interface

    result_content = prepare_for_service(filled_content, service_interface.services_user, sent_mail.tracking_code)

    subject = get_subject_for contact

    mail_data = {
      sent_mail_id: sent_mail.id,
      email:        contact.email,
      subject:      subject,
      body:         result_content
    }
  end

  protected

  def create_initial_sent_mail(contact, service_interface)
    @sent_mail_helper.create_initial_sent_mail(
      service_id: service_interface.services_user.id,
      contact_id: contact.id,
      email:      contact.email
    )
  end

  def get_content_for(contact, content_template = nil)
    insert_data_by_tokens(contact, content_template || @content)
  end

  def get_subject_for(contact, subject_template = nil)
    insert_data_by_tokens(contact, subject_template || @subject)
  end

  private


  def insert_data_by_tokens(contact, template)
    @columns.each do |column|
      unless contact[column[:label].parameterize.underscore.to_sym].blank? || column[:token].blank?
        #byebug
        template = template.to_s.gsub(column[:token].to_s, contact[column[:label].parameterize.underscore.to_sym].to_s)
      else
        template = template.to_s.gsub(column[:token],"")
      end
    end

    template
  end

  def get_columns
    @columns = ListField.find_by_sql(["SELECT '{{' || label || '}}' AS ""token"", label, id, list_id,
      list_field_group_id FROM list_fields WHERE list_id = ? ORDER BY list_fields.id;", @list.id])
  end

  def prepare_for_service(content, service_user, tracking_code)
    service_type = service_user.service.service_type

    footer = footer_for tracking_code
    content = replace_links_in_message content, tracking_code

    if ["email", "smtp_email"].include? service_type.name
      service_mandril = service_user.name == 'mandrill'
      return <<-CONTENT
      <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
      <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>Email made easy, with Klickrr!</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      </head>
      <body>
      <img src="http://www.klickrr.net/open.png?tc=#{tracking_code}" #{"mc:disable-tracking" if service_mandril} />
      #{content}
      #{footer}
      </body>
      </html>
      CONTENT
    end

    content.gsub("=3D", "")
    # content
  end

  def footer_for(tracking_code)
    "<hr style=\"margin: 15px 0 15px 0;\"/><div style=\"text-align: center; font-size: 0.75em\"><a href=\"http://klickrr.net/unsubscribe/me/#{tracking_code}\" target=\"_BLANK\">Please remove me from this list.</a>&nbsp;&bull;&nbsp;Provided by <a target=\"_BLANK\" href=\"klickrr.net\">Klickrr.net</a>.</div>"
  end

  def replace_links_in_message(msg, tracking_code)
    message = Nokogiri::HTML.fragment(msg)
    if tracking_code.include? "=3D"
      tracking_code == tracking_code.unpack('M')[0]
    end

    message.css('a[href]').each do |link|
    #################################################
    ## TODO: Change to SSL when implemented.
    #################################################
      link["href"] = "http://www.klickrr.net/lnks/#{tracking_code}?url=#{link['href']}"
    end

    return message.to_html
  end

end
