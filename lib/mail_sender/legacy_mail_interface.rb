require 'json'

class MailSender::LegacyMailInterface
  attr_reader :services_user
  def initialize(services_user)
    @services_user = services_user

    @service_config = JSON.parse @services_user.usage
    service = Service.find @services_user.service_id
    @service_config['service'] = service.name
    @is_smtp = service.is_smtp?
  end

  def send(data)
    config = {}
    config['sent_mail_id'] = data[:sent_mail_id]
    config['to_email']     = data[:email]
    config['subject']      = data[:subject]
    config['content']      = data[:body]
    config.merge! @service_config

    @is_smtp ? SendingApi.send_smtp(config) : SendingApi.send_api(config)
  end
end
