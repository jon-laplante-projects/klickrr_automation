class MailSender::ContactLoader
  include ContactFilters
  LIMIT = 1000

  def initialize(args)
    @list             = args[:list]
    @message_id       = args[:message_id]
    @message_type     = args[:message_type]
    @sent_mail_helper = args[:sent_mail_helper]
    @segments         = args[:segments]

    @offset = 0
    get_groups
    next_group
  end

  def get_batch(limit = nil)
    limit ||= LIMIT

    contacts = ListValue.find_by_sql(["SELECT * from crosstab('SELECT set_id, label, field_value
      FROM list_fields LEFT OUTER JOIN list_values ON (list_values.list_field_id = list_fields.id)
      WHERE list_values.list_field_group_id = ? ORDER BY list_values.id')
      AS ct(#{@column_string}) OFFSET #{@offset} LIMIT #{limit}", @current_group_id])

    return next_group ? get_batch(limit) : nil if contacts.blank?

    @offset += contacts.count

    filter contacts
  end

  private

  def get_groups
    @groups = ListField.find_by_sql(['SELECT DISTINCT list_field_group_id FROM list_fields WHERE list_id = ?
      ORDER BY list_field_group_id ASC;', @list.id]).map(&:list_field_group_id)
  end

  def filter(contacts)
    emails_contacts = []
    set_ids = []
    contacts.each do |contact|
      emails_contacts << contact.email
      set_ids << contact.set_id
    end

    unsubscribed_set_ids = get_unsubscribed_set_ids(set_ids)
    mails_already_sent = get_mails_already_sent(emails_contacts)

    contacts.select do |contact|
      if mails_already_sent.index(contact.email)
        @sent_mail_helper.mail_already_sent contact
        next
      end

      in_segment?(contact) && !unsubscribed_set_ids.index(contact.set_id)
    end

    # contacts.select do |contact|
    #   if mail_already_sent?(contact)
    #     @sent_mail_helper.mail_already_sent contact
    #     next
    #   end
    #
    #   in_segment?(contact) && !unsubscribed?(contact)
    # end
  end

  def next_group
    @current_group_id = @groups.shift
    @offset = 0
    set_columns

    @current_group_id
  end

  def set_columns
    @columns = ListField.find_by_sql(['SELECT label FROM list_fields LEFT OUTER JOIN
      list_values ON (list_values.list_field_id = list_fields.id)
      WHERE list_values.list_field_group_id = ? ORDER BY list_values.id', @current_group_id])

    cols = Array.new

    @columns.each do |c|
      cols << c.label unless cols.include? c.label
    end

    col_string = 'set_id int'
    cols.each { |col| col_string.concat(", #{col} text") }

    @column_string = col_string
  end
end
