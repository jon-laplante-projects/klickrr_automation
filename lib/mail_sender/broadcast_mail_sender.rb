class MailSender::BroadcastMailSender < MailSender::ListMailSender

  # args:
  #   message_id
  #   user_id
  def initialize(args)
    @message_type = 'broadcast'

    @message_id = args[:message_id]
    @broadcast = Broadcast.includes(:segments).find(@message_id)

    @subject = @broadcast.subject
    @content = @broadcast.content

    @list = @broadcast.list

    @sent_count = 0

    @segments = get_segments

    super(args)
  end

  def perform
    @broadcast.update(state: :processing)
    super
    @broadcast.update(sent: Time.now, state: :completed)
  end

  protected

  def after_sent
    @sent_count += 1
    @broadcast.update(sent_count: @sent_count)
  end

  private

  def get_segments
    segments = []
    @broadcast.segments.each do |segment|
      segments = [segments, segment.get_emails].reject(&:empty?).reduce(:&)
    end
    segments
  end
end
