class MailSender::Base
  def initialize(args)
    raise NotImplementedError
  end

  def perform
    raise NotImplementedError
  end
end
