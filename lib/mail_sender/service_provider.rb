class MailSender::ServiceProvider
  def initialize(args)
    @user_id  = args[:user_id]
    @list     = args[:list]
    @rotating = args[:rotating]

    get_services
  end

  def get_service
    service = @services.shift
    @services.push service

    service
  end

  private

  def get_services
    sql = 'SELECT services.id, name, usage, active, service_id, service_type_id, used FROM
      services_users LEFT OUTER JOIN services ON (services.id = service_id) WHERE user_id = ? AND used = ?'

    sql += "AND services.id IN(#{@list.services})" if @list.services

    @services = ServicesUser.find_by_sql([sql, @user_id, true])
  end
end
