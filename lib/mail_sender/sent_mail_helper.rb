class MailSender::SentMailHelper
  # args:
  #   message_id
  #   message_type
  #   user_id
  def initialize(args)

    @default_attributes = args
  end

  def mail_already_sent(contact)
    sent_mail_attributes = @default_attributes.merge(
      contact_id:       contact.id,
      email:            contact.email,
      sent:             false,
      response_message: 'This contact has already been sent this message.'
    )

    SentMail.create sent_mail_attributes
  end

  # args:
  #   service_id
  #   contact_id
  #   email
  def create_initial_sent_mail(args)
    sent_mail_attributes = @default_attributes.merge(args).merge(sent: false)

    sent_mail = SentMail.new sent_mail_attributes
    sent_mail.generate_token("tracking_code", 12)

    sent_mail.save
    sent_mail
  end
end
