source 'https://rubygems.org'
ruby '2.2.1'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.0'
# Use sqlite3 as the database for Active Record
# gem 'sqlite3'
gem 'pg'
# Use SCSS for stylesheets
# gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
# gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
# gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
# gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
# gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

gem 'whenever', :require => false # Ruby/Rails CRON builder.
gem 'sidekiq' # Delayed job queue.
gem 'sinatra', require: false # Sinatra web framework, included for Sidekiq monitor website.
gem 'slim' # Slim web server, included for Sidekiq monitoring website.
gem 'foreman' # Start/stop multiple services together. Exports to Upstart.
gem 'httparty'
gem 'rest-client'
# Gems for connection to transactional APIs.
gem 'sendgrid-ruby'
gem 'aws-ses'
gem 'mandrill-api', require: 'mandrill'
gem 'mailgun-ruby', require: 'mailgun'
gem 'mailjet' #, :git => 'https://github.com/avbrychak/mailjet-gem.git'
#gem 'postmark'
gem 'postageapp'
gem 'socketlabs'
gem 'rubysl-resolv' # For resolving DNS conflicts.
gem 'valid_email' # Email validation.
gem 'aws-sdk', '~> 2'

#####################################
## Spam score checker.
#####################################
gem 'postmark_spamcheck'

#####################################
## SMTP mail delivery.
#####################################
gem 'mail'

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
end
