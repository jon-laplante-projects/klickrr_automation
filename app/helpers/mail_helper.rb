module MailHelper
  include ApplicationHelper
  def triggers_test(event_type, sent_mail_id)
    sent_mail = SentMail.find(sent_mail_id)
    case event_type
      when "processed"
        # Placeholder.
      when "dropped"
        # Placeholder.
      when "delivered"
        # Placeholder.
      when "deferred"
        # Placeholder.
      when "bounce"
        # Placeholder.
      when "open"
        # sent_mail.update_attribute(:opened, Time.now)
        trigger_arr = [3, 4]

      when "click"
        # sent_mail.save_click('http://example.com')
        trigger_arr = [2, 8, 9]

      when "spamreport"
        # Placeholder.
      when "unsubscribe"
        # sent_mail.update_attribute(:unsubscribed, Time.now)
        trigger_arr = [6]

      when "group_unsubscribe"
        # Placeholder.
        trigger_arr = [6]
      when "group_resubscribe"
        # Placeholder.
    end
    unless trigger_arr.blank?
      # Get the list/bc/etc. id.
      if sent_mail.message_type = "broadcast"
        broadcast = Broadcast.select(:id, :list_id).find(sent_mail.message_id)
        followup = nil
        list_id = broadcast.list_id
      else
        broadcast = nil
        followup = nil
        list_id = followup.list_id
      end
      open_events = Followup.where(["active IS TRUE AND list_id = ? AND trigger_id IN (?)", list_id, trigger_arr])

      unless open_events.blank?
        triggers = FollowupTrigger.where(["id IN (?)", open_events.map { |o| o.trigger_id }])

        # Run the trigger code to push eligible messages into the fu queue.
        triggers.each do |trigger|
          # Fire the followup triggers in application_controller.rb
          followup_triggers(trigger.id, sent_mail_id, list_id)
        end
      end
    end
  end

  def followup_triggers(trigger_id, sent_mail_id, list_id)
    case trigger_id
      when 1
        # No action on sent email in {{x}} days.
        sent_mail = SentMail.find(sent_mail_id)

        Followup.where(trigger_id: 1, active: true, message_type: sent_mail.message_type, message_id: sent_mail.message_id).find_each do |followup|
          sent_followups = SentEmailFollowup.where(followup_id: followup.id).pluck(:sent_mail_id)
          variables = read_vars(followup.trigger_variables)
          unless sent_followups.blank?
            sent_during_range = followup.sent_mails.where(["message_type = 'broadcast' AND message_id = ? AND clicks IS NULL AND opened IS NULL AND dropped IS NULL AND bounced IS NULL AND unsubscribed IS NULL AND spam IS NULL AND blocked IS NULL AND sent_mails.created_at < ? AND sent_mails.email NOT IN (SELECT queued_messages.addr FROM queued_messages WHERE followup_id = ?) AND sent_mails.id NOT IN (?)", followup.message_id, DateTime.now - variables["x"].to_i.days, followup.id, sent_followups])
          else
            sent_during_range = followup.sent_mails.where(["message_type = 'broadcast' AND message_id = ? AND clicks IS NULL AND opened IS NULL AND dropped IS NULL AND bounced IS NULL AND unsubscribed IS NULL AND spam IS NULL AND blocked IS NULL AND sent_mails.created_at < ? AND sent_mails.email NOT IN (SELECT queued_messages.addr FROM queued_messages WHERE followup_id = ?)", followup.message_id, DateTime.now - variables["x"].to_i.days, followup.id])
          end
              if sent_during_range.present?
                sent_during_range.each do |sent|
                  followup.followup_message_parts.each do |msg|
                    queued_message = QueuedMessage.new
                    queued_message.addr = sent.email
                    queued_message.followup_id = followup.id
                    queued_message.subject = msg.subject
                    queued_message.message = msg.message
                    queued_message.send_on = Time.now
                    queued_message.save

                    sent_email_followups = SentEmailFollowup.new
                    sent_email_followups.sent_mail_id = sent.id
                    sent_email_followups.followup_id = followup.id
                    sent_email_followups.sent_at = DateTime.now
                    sent_email_followups.save
                  end
                end
              end
          end

      when 2
        # Link clicked.
        sent_mail = SentMail.find(sent_mail_id)

        Followup.where(trigger_id: 2, active: true, message_type: sent_mail.message_type, message_id: sent_mail.message_id).find_each do |followup|
          sent_followups = SentEmailFollowup.where(followup_id: followup.id).pluck(:sent_mail_id)
          unless sent_followups.blank?
            clicked = followup.sent_mails.where(["sent_mails.id = ? AND clicks IS NOT NULL AND opened IS NOT NULL AND dropped IS NULL AND bounced IS NULL AND unsubscribed IS NULL AND spam IS NULL AND blocked IS NULL AND sent_mails.email NOT IN (SELECT queued_messages.addr FROM queued_messages WHERE followup_id = ?) AND sent_mails.id NOT IN (?)", sent_mail.id, followup.id, sent_followups])
          else
            clicked = followup.sent_mails.where(["sent_mails.id = ? AND clicks IS NOT NULL AND opened IS NOT NULL AND dropped IS NULL AND bounced IS NULL AND unsubscribed IS NULL AND spam IS NULL AND blocked IS NULL AND sent_mails.email NOT IN (SELECT queued_messages.addr FROM queued_messages WHERE followup_id = ?)", sent_mail.id, followup.id])
          end
          if clicked.present?
            followup.followup_message_parts.each do |msg|
              queued_message = QueuedMessage.new
              queued_message.addr = followup.user.email
              queued_message.followup_id = followup.id
              queued_message.subject = msg.subject
              queued_message.message = msg.message
              queued_message.send_on = Time.now
              queued_message.save

              sent_email_followups = SentEmailFollowup.new
              sent_email_followups.sent_mail_id = sent_mail_id
              sent_email_followups.followup_id = followup.id
              sent_email_followups.sent_at = DateTime.now
              sent_email_followups.save
            end
          end
        end

      when 3
        # Email opened, no links clicked.
        sent_mail = SentMail.find(sent_mail_id)

        Followup.where(trigger_id: 3, active: true, message_type: sent_mail.message_type, message_id: sent_mail.message_id).find_each do |followup|
          sent_followups = SentEmailFollowup.where(followup_id: followup.id).pluck(:sent_mail_id)
          unless sent_followups.blank?
            sent_mails = followup.user.sent_mails.where(["sent_mails.id = ? AND opened IS NOT NULL AND clicks IS NULL AND unsubscribed IS NULL AND blocked IS NULL AND spam IS NULL AND dropped IS NULL AND sent_mails.email NOT IN (SELECT queued_messages.addr FROM queued_messages WHERE followup_id = ?) AND sent_mails.id NOT IN (?)", sent_mail.id, followup.message_id, sent_followups])
          else
            sent_mails = followup.user.sent_mails.where(["sent_mails.id = ? AND opened IS NOT NULL AND clicks IS NULL AND unsubscribed IS NULL AND blocked IS NULL AND spam IS NULL AND dropped IS NULL AND sent_mails.email NOT IN (SELECT queued_messages.addr FROM queued_messages WHERE followup_id = ?)", sent_mail.id, followup.message_id])
          end
          if sent_mails.present?
            followup.followup_message_parts.each do |msg|
              queued_message = QueuedMessage.new
              queued_message.addr = followup.user.email
              queued_message.followup_id = followup.id
              queued_message.subject = msg.subject
              queued_message.message = msg.message
              queued_message.send_on = Time.now
              queued_message.save

              sent_email_followups = SentEmailFollowup.new
              sent_email_followups.sent_mail_id = sent_mail_id
              sent_email_followups.followup_id = followup.id
              sent_email_followups.sent_at = DateTime.now
              sent_email_followups.save
            end
          end
        end

      when 4
        # Email opened within {{x}} days.
        sent_mail = SentMail.find(sent_mail_id)

        Followup.where(trigger_id: 4, active: true, message_type: sent_mail.message_type, message_id: sent_mail.message_id).find_each do |followup|
          sent_followups = SentEmailFollowup.where(followup_id: followup.id).pluck(:sent_mail_id)
          variables = read_vars(followup.trigger_variables)
          unless sent_followups.blank?
            sent_mails = followup.user.sent_mails.where(["sent_mails.id = ? AND sent_mails.created_at < ? AND opened IS NOT NULL AND clicks IS NULL AND unsubscribed IS NULL AND blocked IS NULL AND spam IS NULL AND dropped IS NULL AND sent_mails.email NOT IN (SELECT queued_messages.addr FROM queued_messages WHERE followup_id = ?) AND sent_mails.id NOT IN (?)", sent_mail.id, Time.now - variables['x'].to_i.days, followup.id, sent_followups])
          else
            sent_mails = followup.user.sent_mails.where(["sent_mails.id = ? AND sent_mails.created_at < ? AND opened IS NOT NULL AND clicks IS NULL AND unsubscribed IS NULL AND blocked IS NULL AND spam IS NULL AND dropped IS NULL AND sent_mails.email NOT IN (SELECT queued_messages.addr FROM queued_messages WHERE followup_id = ?)", sent_mail.id, Time.now - variables['x'].to_i.days, followup.id])
          end
          if sent_mails.present?
            followup.followup_message_parts.each do |msg|
              queued_message = QueuedMessage.new
              queued_message.addr = followup.user.email
              queued_message.followup_id = followup.id
              queued_message.subject = msg.subject
              queued_message.message = msg.message
              queued_message.send_on = Time.now
              queued_message.save

              sent_email_followups = SentEmailFollowup.new
              sent_email_followups.sent_mail_id = sent_mail_id
              sent_email_followups.followup_id = followup.id
              sent_email_followups.sent_at = DateTime.now
              sent_email_followups.save
            end
          end
        end

      when 5
        # New subscription.

      when 6
        # Subscriber unsubscribed from list.
        sent_mail = SentMail.find(sent_mail_id)

        Followup.where(trigger_id: 6, active: true, message_type: sent_mail.message_type, message_id: sent_mail.message_id).find_each do |followup|
          sent_followups = SentEmailFollowup.where(followup_id: followup.id).pluck(:sent_mail_id)
          unless sent_followups.blank?
            sent_mails = followup.user.sent_mails.where(["sent_mails.id = ? AND unsubscribed IS NOT NULL AND sent_mails.email NOT IN (SELECT queued_messages.addr FROM queued_messages WHERE followup_id = ?) AND sent_mails.id NOT IN (?)", sent_mail.id, followup.id, sent_followups])
          else
            sent_mails = followup.user.sent_mails.where(["sent_mails.id = ? AND unsubscribed IS NOT NULL AND sent_mails.email NOT IN (SELECT queued_messages.addr FROM queued_messages WHERE followup_id = ?)", sent_mail.id, followup.id])
          end
          if sent_mails.present?
            followup.followup_message_parts.each do |msg|
              queued_message = QueuedMessage.new
              queued_message.addr = followup.user.email
              queued_message.followup_id = followup.id
              queued_message.subject = msg.subject
              queued_message.message = msg.message
              queued_message.send_on = Time.now
              queued_message.save

              sent_email_followups = SentEmailFollowup.new
              sent_email_followups.sent_mail_id = sent_mail_id
              sent_email_followups.followup_id = followup.id
              sent_email_followups.sent_at = DateTime.now
              sent_email_followups.save
            end
          end
        end

      when 8
        # {{x}} or more links in an email clicked within {{y}} days.
        sent_mail = SentMail.find(sent_mail_id)

        # Check to make sure this is a broadcast.
        if sent_mail.message_type.downcase === "broadcast"

          Followup.where(trigger_id: 8, active: true, message_type: sent_mail.message_type, message_id: sent_mail.message_id).find_each do |followup|
            sent_followups = SentEmailFollowup.where(followup_id: followup.id).pluck(:sent_mail_id)
            variables = read_vars(followup.trigger_variables)

            if JSON.parse(sent_mail.clicks).count >= variables["x"].to_i && ((Time.now - sent_mail.created_at)/24/60/60).to_i <= variables["y"].to_i
              unless sent_followups.blank?
                sent_mails = followup.user.sent_mails.where(["sent_mails.id = ? AND clicks IS NOT NULL AND dropped IS NULL AND bounced IS NULL AND unsubscribed IS NULL AND spam IS NULL AND blocked IS NULL AND sent_mails.email NOT IN (SELECT queued_messages.addr FROM queued_messages WHERE followup_id = ?) AND sent_mails.id NOT IN (?)", sent_mail.id, followup.id, sent_followups])
              else
                sent_mails = followup.user.sent_mails.where(["sent_mails.id = ? AND clicks IS NOT NULL AND dropped IS NULL AND bounced IS NULL AND unsubscribed IS NULL AND spam IS NULL AND blocked IS NULL AND sent_mails.email NOT IN (SELECT queued_messages.addr FROM queued_messages WHERE followup_id = ?)", sent_mail.id, followup.id])
              end
              if sent_mails.present?
                followup.followup_message_parts.each do |msg|
                  queued_message = QueuedMessage.new
                  queued_message.addr = followup.user.email
                  queued_message.followup_id = followup.id
                  queued_message.subject = msg.subject
                  queued_message.message = msg.message
                  queued_message.send_on = Time.now
                  queued_message.save

                  sent_email_followups = SentEmailFollowup.new
                  sent_email_followups.sent_mail_id = sent_mail_id
                  sent_email_followups.followup_id = followup.id
                  sent_email_followups.sent_at = DateTime.now
                  sent_email_followups.save
                end
              end
            end
          end
        end

      when 9
        sent_mail = SentMail.find(sent_mail_id)

        # {{x}} or more links in an email clicked after {{y}} days.
        Followup.where(trigger_id: 9, active: true, message_type: sent_mail.message_type, message_id: sent_mail.message_id).find_each do |followup|
          sent_followups = SentEmailFollowup.where(followup_id: followup.id).pluck(:sent_mail_id)
          variables = read_vars(followup.trigger_variables)
          if JSON.parse(sent_mail.clicks).count >= variables["x"].to_i && ((Time.now - sent_mail.created_at)/24/60/60).to_i > variables["y"].to_i
            unless sent_followups.blank?
              sent_mails = followup.user.sent_mails.where(["sent_mails.id = ? AND clicks IS NOT NULL AND dropped IS NULL AND bounced IS NULL AND unsubscribed IS NULL AND spam IS NULL AND blocked IS NULL AND sent_mails.email NOT IN (SELECT queued_messages.addr FROM queued_messages WHERE followup_id = ?) AND sent_mails.id NOT IN (?)", sent_mail.id, followup.id, sent_followups])
            else
              sent_mails = followup.user.sent_mails.where(["sent_mails.id = ? AND clicks IS NOT NULL AND dropped IS NULL AND bounced IS NULL AND unsubscribed IS NULL AND spam IS NULL AND blocked IS NULL AND sent_mails.email NOT IN (SELECT queued_messages.addr FROM queued_messages WHERE followup_id = ?)", sent_mail.id, followup.id])
            end
            if sent_mails.present?
              followup.followup_message_parts.each do |msg|
                queued_message = QueuedMessage.new
                queued_message.addr = followup.user.email
                queued_message.followup_id = followup.id
                queued_message.subject = msg.subject
                queued_message.message = msg.message
                queued_message.send_on = Time.now
                queued_message.save

                sent_email_followups = SentEmailFollowup.new
                sent_email_followups.sent_mail_id = sent_mail_id
                sent_email_followups.followup_id = followup.id
                sent_email_followups.sent_at = DateTime.now
                sent_email_followups.save
              end
            end
          end
        end

      when 10
        # {{x}} days since broadcast was sent.
        Followup.where(trigger_id: 10, active: true, message_type: sent_mail.message_type, message_id: sent_mail.message_id).find_each do |followup|
          sent_followups = SentEmailFollowup.where(followup_id: followup.id).pluck(:sent_mail_id)
          variables = read_vars(followup.trigger_variables)
          unless sent_followups.blank?
            sent_during_range = followup.sent_mails.where(["message_type = 'broadcast' AND message_id = ? AND dropped IS NULL AND bounced IS NULL AND unsubscribed IS NULL AND spam IS NULL AND blocked IS NULL AND sent_mails.created_at::DATE = ?::DATE AND sent_mails.email NOT IN (SELECT queued_messages.addr FROM queued_messages WHERE followup_id = ?) AND sent_mails.id NOT IN (?)", followup.message_id, DateTime.now - variables["x"].to_i.days, followup.id, sent_followups])
          else
            sent_during_range = followup.sent_mails.where(["message_type = 'broadcast' AND message_id = ? AND dropped IS NULL AND bounced IS NULL AND unsubscribed IS NULL AND spam IS NULL AND blocked IS NULL AND sent_mails.created_at::DATE = ?::DATE AND sent_mails.email NOT IN (SELECT queued_messages.addr FROM queued_messages WHERE followup_id = ?)", followup.message_id, DateTime.now - variables["x"].to_i.days, followup.id])
          end
          if sent_during_range.present?
            sent_during_range.each do |sent|
              followup.followup_message_parts.each do |msg|
                queued_message = QueuedMessage.new
                queued_message.addr = sent.email
                queued_message.followup_id = followup.id
                queued_message.subject = msg.subject
                queued_message.message = msg.message
                queued_message.send_on = Time.now
                queued_message.save

                sent_email_followups = SentEmailFollowup.new
                sent_email_followups.sent_mail_id = sent.id
                sent_email_followups.followup_id = followup.id
                sent_email_followups.sent_at = DateTime.now
                sent_email_followups.save
              end
            end
          end
        end

    end
  end
end
