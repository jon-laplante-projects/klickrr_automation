require_relative '../../lib/mail_tasks'

class AutomationWorker
  include Sidekiq::Worker

  def perform_async(message_type)
    # puts "In AutomationWorker..."
    # puts "Current UTC time: #{Time.now.utc}"
    if message_type.downcase == "broadcast"
      bc = Broadcast.where("sent IS NULL AND utc_push_at < '#{Time.now.utc}'")

      bc.each do |broadcast|
        # User id.
        if broadcast.list.services.present?
          user_id = broadcast.list.user_id

          args = { user_id: user_id }

    	  	mail_tasks = MailTasks.new
          mail_tasks.message_sender(broadcast.id, "broadcast", args)

          broadcast.sent = Time.now
          broadcast.save
        else
          Rails.logger.info "Can not send broadcast with id = #{broadcast.id}. No active services in list."  
        end
      end
    else
      # Followups.
      [1,10].each do |trigger|
        followup_code = FollowupTrigger.find(trigger).code

        #Thread.new do
          eval(followup_code)
        #end
      end
    end
  end
end
