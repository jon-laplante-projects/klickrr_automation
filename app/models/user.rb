# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime
#  updated_at             :datetime
#  trans_type             :string(255)
#  hash_key               :string(255)
#  trans_gateway          :string(255)
#  trans_date             :datetime
#  trans_testmode         :boolean
#  seller_id              :string(255)
#  seller_email           :string(255)
#  name                   :string(255)
#  first_name             :string(255)
#  last_name              :string(255)
#  country                :string(255)
#  status                 :string(255)
#  mobile_phone           :string
#

class User < ActiveRecord::Base
  before_save :add_default_data

  has_many :lists
  has_many :followups, dependent: :destroy
  has_many :broadcasts, dependent: :destroy
  has_many :sent_mails, dependent: :destroy
  has_many :services_users, dependent: :destroy

  def add_default_data
    unless (self.first_name.blank? && self.last_name.blank?) || self.name != nil
      self.name = self.first_name.to_s + " " + self.last_name.to_s

      self.save
    end
  end
end