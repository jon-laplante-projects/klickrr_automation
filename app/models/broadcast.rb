class Broadcast < ActiveRecord::Base
  enum state: [:sheduled, :processing, :completed]
  
  belongs_to :list
  # has_many :broadcast_followups, dependent: :destroy
  # has_many :broadcast_segments, dependent: :destroy
  has_many :segments, as: :message, dependent: :destroy
  has_many :opened_mail, as: :message, dependent: :destroy
  has_many :sent_mails, as: :message, dependent: :destroy

  def self.create_test_broadcast(list_id, attrs)
    bc = Broadcast.new(list_id: list_id, test_type: attrs[:type])

    bc.subject = attrs[:title]
    bc.content = attrs[:body]
    bc.timezone = attrs[:timezone]
    bc.gmtAdjustment = attrs[:gmtAdjustment]


    unless attrs[:send_on].blank?
      bc.push_at = attrs[:send_on].to_datetime
      
      # Add the timezone to the send time and save as UTC.
      utc_push_at = attrs[:send_on].to_datetime
      utc_push_at = utc_push_at - bc.gmtAdjustment.hour

      bc.utc_push_at = utc_push_at
    end
    bc.save

    # unless attrs[:segment_ids].blank?
    #   save_segments("broadcast", bc.id, attrs[:segment_ids])
    # end
    bc
  end  

end