require 'net/http'
# require 'SmtpApiHeader.rb'

class MailTasks
	def initialize()

	end

	##############################################################
	##############################################################
	def test_whenever
		# File("test.txt", "w") do |f|
		#  	f.write("Another test at #{Time.now}.")
		#  end
		puts "Test Wheneverize!"
	end
	##############################################################
	##############################################################

	def find_scheduled_broadcasts
		Broadcast.where("sent IS NULL AND push_at < '#{Time.now}'")
	end

	def find_scheduled_followups

	end


	# sent is null
	# push_at passed.

	def message_sender(id_val, send_type_val, args)
  	#################################################
	## TODO: Move this, send_smtp and send_api into
	## their own class and simply call them from
	## MailController.
	#################################################
		#Thread.new do
			config = Hash.new
			# Determine what time of mailing this is, and pull data as is appropriate.
			case "#{send_type_val}"
				when "broadcast"
					bc = Broadcast.find(id_val)

					config["subject"] = bc.subject
					master_content = bc.content

					list = bc.list

					if bc.broadcast_segments.count > 0
						seg_sql = "SELECT DISTINCT EMAIL FROM sent_mails WHERE user_id = #{bc.list.user_id} AND sent = true AND unsubscribed IS NULL "
						bc.broadcast_segments.each do |seg|
							seg_type = SegmentType.find(seg.segment_id)
							seg_sql = seg_sql += " AND #{seg_type.base_sql}"
						end
						seg_sql = seg_sql.gsub("{{list_id}}", list.id.to_s)
						seg_sql = seg_sql.gsub("{{user_id}}", list.user_id.to_s)
						segments = SentMail.find_by_sql("#{seg_sql};").pluck()
					else
						segments = nil
					end

				when "followup"
					queued = QueuedMessage.find(id_val)

					config["subject"] = queued.subject
					master_content = queued.message

					email_val = Hash.new
					email_val["email"] = queued.addr

					list = email_val.to_json

				when "conn_test"
					config["subject"] = "Klickrr :: Testing Service Connection"
					master_content = "This is a test of the selected service in Klickrr."

					service = args["service_id"]

					email_val = Hash.new

					email_val["email"] = args["email"]

					list = email_val.to_json

				when "email_preview"
					# Put together the email preview var inits.
					config["subject"] = "#{args["subject"]}"
					master_content = "#{args["content"]}"

					email = Hash.new

					email["email"] = email_val

					list = email.to_json
			end

			#unless args["user_id"].blank?
			config["user_id"] = args[:user_id]
			#end

			# Set our content variable - we won't touch master_content.
			content = master_content

			# Because this is using the old 'data bag' model, we need to
			# get the column name/label order from list_values and then
			# match them up the the labels in list_fields.
			list_groups = ListField.find_by_sql(["SELECT DISTINCT list_field_group_id FROM list_fields WHERE list_id = ? ORDER BY list_field_group_id ASC;", list.id])
			list_groups.each do |group|
				columns = ListField.find_by_sql(["SELECT label FROM list_fields LEFT OUTER JOIN list_values ON (list_values.list_field_id = list_fields.id) WHERE list_values.list_field_group_id = ? ORDER BY list_values.id", group.list_field_group_id])
				#columns = ListField.find_by_sql(["SELECT label FROM list_fields LEFT OUTER JOIN list_values ON (list_values.list_field_id = list_fields.id) WHERE list_id = ? ORDER BY list_values.id", list.id])
		  		cols = Array.new

		  		columns.each do |c|
		  			cols << c.label unless cols.include? c.label
		  		end

				col_string = "set_id int"
				cols.each do |col|
					col_string.concat(", #{col} text")
				end

				#################################################
				## TODO: Figure out a way to get all columns, while
				## honoring the correct order, and still
				## identifying email addresses.
				#################################################

				# #contacts = ListValue.find_by_sql(["SELECT * from crosstab('SELECT set_id, label, field_value FROM list_fields LEFT OUTER JOIN list_values ON (list_values.list_field_id = list_fields.id) WHERE list_id = ? ORDER BY list_values.id') AS ct(set_id int, first_name text, last_name text, email text, phone text)", list.id])
				# contacts = ListValue.find_by_sql(["SELECT * from crosstab('SELECT set_id, label, field_value FROM list_fields LEFT OUTER JOIN list_values ON (list_values.list_field_id = list_fields.id) WHERE list_values.list_field_group_id = ? ORDER BY list_values.id') AS ct(#{col_string})", group.list_field_group_id])
				# #puts "#{contacts.to_json}"
				# #contacts = ListValue.find_by_sql("SELECT * from crosstab('SELECT set_id, label, field_value FROM list_fields LEFT OUTER JOIN list_values ON (list_values.list_field_id = list_fields.id) WHERE list_id = #{params[:list_id].to_i} AND set_id IN (#{record_sets.join(",")}) ORDER BY list_values.id') AS ct(#{col_string})")

				usr = list.user

				if list.services.blank?
					services = ServicesUser.find_by_sql(["SELECT services.id, name, usage, active, service_id FROM services_users LEFT OUTER JOIN services ON (services.id = service_id) WHERE active = ? AND user_id = ?", true, usr.id])
				else
					services = ServicesUser.find_by_sql(["SELECT services.id, name, usage, active, service_id FROM services_users LEFT OUTER JOIN services ON (services.id = service_id) WHERE active = ? AND user_id = ? AND services.id IN(#{ list.services })", true, usr.id])
				end

				# Set up services to rotate through each that is active.
				service_index = 0

				api_key = "api_key"
				api_name = "api_name"

				json_arr = Array.new

				columns = ListField.find_by_sql(["SELECT '{{' || label || '}}' AS token, label, id, list_id, list_field_group_id FROM list_fields WHERE list_id = ? ORDER BY list_fields.id;", list.id])

				# We're going to pull batches of 1,000 rows to process.
				offset = 0
				limit = 1000
				contacts = ListValue.find_by_sql(["SELECT * from crosstab('SELECT set_id, label, field_value FROM list_fields LEFT OUTER JOIN list_values ON (list_values.list_field_id = list_fields.id) WHERE list_values.list_field_group_id = ? ORDER BY list_values.id') AS ct(#{col_string}) LIMIT 1000", group.list_field_group_id])

				# As long as we've got contacts, keep batching in 1,000s.
				while contacts.blank? == false
					offset += limit
					contacts.each do |contact|
						# Unless there are segments *AND* the current email is not in the segmented list.
						unless segments.blank? == false && segments.include?(contact.email) == false
							# Let's check and see if there are unsubscribers in this list.
							unsubscribed = Unsubscribe.where(set_id: contact.set_id).size
							unless unsubscribed > 0
								contact.attributes.each do |c|
									#puts "#{c[0]} = #{contact["#{c[0]}"]}"
									config["#{c[0]}"] = "#{contact["#{c[0]}"]}"
								end

								# Check and make sure that this email address hasn't already received  this message.
								sent_mail = SentMail.where(message_id: id_val, message_type: "#{send_type_val}", email: "#{contact.email}").count

								unless sent_mail > 0

									# Get the user's service preferences (i.e. which is active, logins, etc.)
									service_user = ServicesUser.find_by(service_id: services[service_index].id, user_id: list.user_id)
									usage = JSON.parse(service_user.usage)

									usage.each do |key, param|
										config[key] = param
									end

									# If domain, sender, and sender_email are assigned to the list, use those vals instead of service vals.
									unless list.domain.blank?
										config["domain"] = list.domain
									end

									unless list.sender.blank?
										config["sender"] = list.from_email
									end

									unless list.sender_email.blank?
										config["sender_name"] = list.from_name
									end
									# Update the message content - replace {{placeholders}}, add footer, and tracking pixel.
									#tracking_code = SecureRandom.hex(9)
									###################################
									## TODO: Ensure the tracking_code
									## is unique.
									###################################

									# Save this 'sent' to the sent_mail table.
									sent_mail = SentMail.new
										sent_mail.user_id = config["user_id"]
										sent_mail.service_id = services[service_index].id
										sent_mail.message_id = id_val
										sent_mail.contact_id = contact.id
										sent_mail.message_type = send_type_val
										sent_mail.email = contact.email
										sent_mail.generate_token("tracking_code", 12)
									sent_mail.save

									config["sent_mail_id"] = sent_mail.id
									config["to_email"] = "#{config["email"]}"

									columns.each do |column|
										unless contact[column[:label].parameterize.underscore.to_sym].blank?
											content = content.to_s.gsub(column[:token],contact[column[:label].parameterize.underscore.to_sym])
											#content = content.to_s.gsub(column[:token],contact["#{column[:label]}"])
										else
											content = content.to_s.gsub(column[:token],"")
										end
									end

									# Let's get our message built!
									services_desc = Service.find(services[service_index].service_id)

									#puts "Service: #{services_desc.name}"

									if services_desc.is_smtp? || services_desc.name == "ses" || services_desc.name == "postage"
										footer = "<hr style=\"margin: 15px 0 15px 0;\"/><div style=\"text-align: center; font-size: 0.75em\"><a href=\"http://klickrr.net/unsubscribe/me/#{sent_mail.tracking_code}\" target=\"_BLANK\">Please remove me from this list.</a>&nbsp;&bull;&nbsp;Provided by <a target=\"_BLANK\" href=\"klickrr.net\">Klickrr.net</a>.</div>"
									else
										footer = "<div style=\"text-align: center; font-size: 0.75em\">Provided by <a target=\"_BLANK\" href=\"klickrr.net\">Klickrr.net</a>.</div>"
									end

									if services_desc.is_smtp? || services_desc.name == "ses" || services_desc.name == "postage"
										content = replace_links_in_message(content, sent_mail.tracking_code)
									end

									#######################################################
									## TODO: Unsubscribe endpoint can stay the same, but the
									## link will eventually be conditionally and only for
									## services that don't provide there own.
									##
									##
									## TODO: Add conditionals for services with webhooks.
									##
									## Don't forget to handle the tracking pixel
									## appropriately, too.
									#######################################################
									#content = replace_links_in_message(content)

									# Prep our email for delivery.
									message = <<-CONTENT
									<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
									<html xmlns="http://www.w3.org/1999/xhtml">
									<head>
									<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
									<title>Demystifying Email Design</title>
									<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
									</head>
									<body>
									<img src="http://www.klickrr.net/open.png?tc=#{ sent_mail.tracking_code }"#{ "mc:disable-tracking" if services[service_index].name == "mandrill" } />
									#{content}
									#{footer}
									</body>
									</html>
									CONTENT

									#message = message.concat('<hr style="margin: 15px 0 15px 0;"/><div style="text-align: center;">Provided by <a href="klickrr.net">Klickrr.net</a></div>')
									#message = message.concat("</body></html>")

									config["content"] = message

									# Reset content so it's not sending the same message again and again.
									content = master_content

									# Let's get that message sent!
									#services_desc = Service.find(services[service_index].service_id)
									config["service"] = "#{services_desc.name}"
									if services_desc.is_smtp?
										send_smtp(config)
									else
										send_api(config)
									end

									# Rotate service used.
									if service_index == services.count - 1
										service_index = 0
									else
										service_index += 1
									end

									# Don't forget to move SMTP stuff to the API OR call it seperately.
								else
									sent_mail = SentMail.new
										sent_mail.user_id = config["user_id"]
										sent_mail.service_id = services[service_index].id
										sent_mail.message_id = id_val
										sent_mail.contact_id = contact.id
										sent_mail.message_type = "broadcast"
										sent_mail.email = contact.email
										sent_mail.sent = false
										sent_mail.response_message = "This contact has already been sent this message."
									sent_mail.save
								end # Closing UNLESS this message has been sent to this contact...
							end
						end
					end

					# Get the next batch of 1,000 contacts.
					contacts = ListValue.find_by_sql(["SELECT * from crosstab('SELECT set_id, label, field_value FROM list_fields LEFT OUTER JOIN list_values ON (list_values.list_field_id = list_fields.id) WHERE list_values.list_field_group_id = ? ORDER BY list_values.id') AS ct(#{col_string}) OFFSET #{offset} LIMIT #{limit}", group.list_field_group_id])
				end
			end

			unless bc.blank?
				bc.sent = Time.now
			end

			unless queued.blank?
				queued.delivery_on = Time.now
			end
		#end # Close the thread
  	end

	def send_smtp(config)
		unless config.size < 1
			sent_mail = SentMail.find(config["sent_mail_id"])
			#config = Hash.new

			#params.each do |k,v|
				#puts "=========================="
				#puts "Key: #{k}"
		        #puts "Val: #{v}"
		        #config[k] = v
		        #puts "=========================="
	    	#end

	    	begin
				address = config["address"]
				port = config["port"]
				domain = config["domain"]
				user_name = config["user_name"]
				password = config["password"]
				authentication = config["authentication"]
				#enable_starttls_auto = params[:enable_starttls_auto]
				sender = config["from_email"]
				recipient = config["to_email"]
				subject = config["subject"]
				content = config["content"]
				sendmail = config["sendmail"]

				#ActionMailer::Base.delivery_method = :smtp
				#ActionMailer::Base.smtp_settings = {
				#	address:              "#{address}",#'smtp.gmail.com',
				#	port:                 "#{port}",#587,
				#	#domain:               "#{domain}",#'example.com',
				#	user_name:            "#{user_name}",#'<username>',
				#	password:             "#{password}",#'<password>',
				#	authentication:       "plain",#'plain',
				#	#authentication:       "#{authentication}",#'plain',
				#	raise_delivery_errors: true,
				#	enable_starttls_auto: true }

				begin
					mail = Mail.new do
					  from     "#{sender}"
					  to       "#{recipient}"
					  subject  "#{subject}"

					  text_content = "#{ActionController::Base.helpers.sanitize(content)}"
					  text_part do
					  	body "#{text_content}"
					  end

					  html_part do
					  	content_type 'text/html; charset=UTF-8'
					  	body "#{content}"
					  end
					end

					if sendmail == true
						mail.delivery_method :sendmail, address: "#{address}", port: port, domain: "#{domain}", user_name: "#{user_name}", password: "#{password}", authentication: "#{authentication}", enable_starttls_auto: true
					else
						mail.delivery_method :smtp, address: "#{address}", port: port, domain: "#{domain}", user_name: "#{user_name}", password: "#{password}", authentication: "#{authentication}", enable_starttls_auto: true
					end

					mail.deliver!
				#	SmtpMailer.smtp_test(recipient, sender).deliver_now

				#	sent_mail.sent = true
				#	sent_mail.response_message = "#{Net::SMTP::Response} - You have successfully sent a message via SMTP."
				#	sent_mail.save

					return true
				rescue Net::SMTPAuthenticationError, Net::SMTPServerBusy, Net::SMTPSyntaxError, Net::SMTPFatalError, Net::SMTPUnknownError => e
					sent_mail.sent = false
					sent_mail.response_message = "#{e.class} - #{e.message}"
					sent_mail.save

					return false
				end

				sent_mail.sent = true
				sent_mail.response_message = "You have successfully sent a message via SMTP."
				sent_mail.save

				return true
			rescue => e
				sent_mail.sent = false
				sent_mail.response_message = "#{e.class} - #{e.message}"
				sent_mail.save

				return false
			end
		end
	end

	def send_api(config)
		unless config.size < 1
			sent_mail = SentMail.find(config["sent_mail_id"])

			api_key = config["api_key"]
			api_name = config["api_name"]

			#==================================
			# From 'Broadcasts' and 'Followups'
			#==================================
			subject = config["subject"]
			content = config["content"]

			#==================================
			# Attached to 'List'
			#==================================
			recipient = config["to_email"]
			recipient_name = config["to_email"]
			sender = config["from_email"]
			sender_name = config["from_email"]
			service = config["service"]
			domain = config["domain"]

			case service
				when "sendgrid"
					begin
						# Set the external id for this sent_mail record.
						sent_mail.generate_token("ext_id", 12)

						# Set the SMTPAPI header.
						hdr = SmtpApiHeader.new

						receiver = ["#{recipient}"]

						hdr.addTo(receiver)
						hdr.setUniqueArgs({"ext_id" => sent_mail.ext_id})

						# Create and populate the mail obj.
						mail = Mail.new do
							header['X-SMTPAPI'] =  hdr.asJSON()
							from     "#{sender}"
							to       "#{recipient}"
							subject  "#{subject}"

							text_content = "#{ActionController::Base.helpers.sanitize(content)}"
							text_part do
								body "#{text_content}"
							end

							html_part do
								content_type 'text/html; charset=UTF-8'
								body "#{content}"
							end
						end

						# Set the SMTP settings.
						mail.delivery_method :smtp, address: "smtp.sendgrid.net", port: 587, domain: "#{domain}", user_name: "#{api_name}", password: "#{api_key}", authentication: "plain", enable_starttls_auto: true

						# ...and send that mail!
						mail.deliver!

						sent_mail.sent = true
					    sent_mail.response_message = "Message sent via Sendgrid"
					rescue => e
						# puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
						# puts "Nope! Didn't work. #{e.inspect}"
						# puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

						sent_mail.sent = false
					    sent_mail.response_message = "#{e.class} - #{e.message}"

					end

				when "mandrill"
					begin
					    mandrill = Mandrill::API.new "#{api_key}"
					    template_content = [{}]
					    message = {"from_name"=>"#{sender_name}",
					    "metadata"=>{"sent_mail_id"=>"#{sent_mail.id}"},
					    "track_clicks"=>true,
					    "subject"=>"#{subject}",
					    "html"=>"#{content}",
					     "to"=>
					        [{"email"=>"#{recipient}",
					            "name"=>"#{recipient_name}",
					            "type"=>"to"}],
					     "track_opens" => true,
					     "headers"=>{"Reply-To"=>"#{sender}"},
					     "from_email"=>"#{sender}"}
					    async = true
					    ip_pool = "Main Pool"
					    send_at = {}
					    result = mandrill.messages.send message, async, ip_pool, send_at
					        unless result[0]["_id"].blank?
					        	sent_mail.ext_id = result[0]["_id"]
					        end
					        unless result[0]["status"].blank?
					        	sent_mail.status = result[0]["status"]
					        end
					    sent_mail.sent = true
					    sent_mail.response_message = "Message sent via Mandrill"

					rescue => e
					    # Mandrill errors are thrown as exceptions
					    sent_mail.sent = false
					    sent_mail.response_message = "A mandrill error occurred: #{e.class} - #{e.message}"
					    raise
					end

				when "mailjet"
					begin
						#HTTParty.post("https://#{api_key}:#{api_name}@https://api.mailjet.com/v3/send/message")
						Mailjet.configure do |config|
						  config.api_key = "#{api_key}"
						  config.secret_key = "#{api_name}"
						  config.default_from = "#{sender}"
						end

						result = Mailjet::MessageDelivery.create(from: "#{sender}", to: "#{recipient}", subject: "#{subject}", html: "#{content}")


						# puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
						# puts "~~ #{result.inspect}"
						# puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

						unless result.attributes["id"].blank?
							sent_mail.ext_id = result.attributes["id"]
							sent_mail.status = "sent"
						end

						sent_mail.sent = true
						sent_mail.response_message = "Message sent via Mailjet"
					rescue => e
						sent_mail.sent = false
						sent_mail.status = "error"
					    sent_mail.response_message = "#{e.class} - #{e.message}"
					end

				when "mailgun"
					begin
						# First, instantiate the Mailgun Client with your API key
						mg_client = Mailgun::Client.new "#{api_key}"

						# Define your message parameters
						message_params = {:from    => "#{sender}",
						                  :to      => "#{recipient}",
						                  :subject => "#{subject}",
						                  :html    => "#{content}"}

						# Send your message through the client
						result = mg_client.send_message("#{domain}", message_params).to_h!

						unless result["id"].blank?
							sent_mail.ext_id = result["id"]
						end
						unless result["message"].blank?
							sent_mail.response_message = result["message"]
						end

						sent_mail.sent = true
						sent_mail.status = "sent"

					rescue => e
						sent_mail.sent = false
						sent_mail.status = "error"
					    sent_mail.response_message = "#{e.class} - #{e.message}"
					end

				when "ses"
					begin
						ses = AWS::SES::Base.new(
							:access_key_id     => "#{api_name}",
							:secret_access_key => "#{api_key}"
						)

						result = ses.send_email(
							:to        => ["#{recipient}"],
							:source    => "\"#{sender_name}\" <#{sender}>",
							:subject   => "#{subject}",
							:html_body => "#{content}"
						)

						result_hash = Hash.from_xml(result)

						unless result_hash["SendEmailResponse"]["SendEmailResult"]["MessageId"].blank?
							sent_mail.ext_id = "#{result_hash["SendEmailResponse"]["SendEmailResult"]["MessageId"]}"
							sent_mail.status = "sent"
						end

						sent_mail.sent = true
					    sent_mail.response_message = "Message sent via Amazon SES"
					rescue => e
						sent_mail.sent = false
						sent_mail.status = "error"
					    sent_mail.response_message = "#{e.class} - #{e.message}"
					end

				when "postage"
					begin
						result_args = Hash.new
						arguments = Hash.new
						headers = Hash.new
						content_hash = Hash.new

						content_hash["text/html"] = "#{content}"

						headers["subject"] =  "#{subject}"
						headers["from"] =  "#{sender}"

						arguments["recipients"] = ["#{recipient}"]
						arguments["headers"] = headers
						arguments["content"] = content_hash

						result_args["api_key"] = "#{api_key}"
						result_args["arguments"] = arguments

						# puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
						# puts "JSON: #{result_args.to_json}"
						# puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
						# puts "Content: #{content}"
						# puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

						result = HTTParty.post("https://api.postageapp.com/v.1.0/send_message.json",
				    	:body => result_args.to_json,
				    	:headers => { 'Content-Type' => 'application/json' } )

						if result["response"]["status"] == "ok"
							sent_mail.sent = true
							sent_mail.status = "sent"
					    	sent_mail.response_message = "Message sent via Postage"

					    	unless result["data"]["message"]["id"].blank?
					    		sent_mail.ext_id = result["data"]["message"]["id"]
					    	end
						else
							sent_mail.sent = false
							sent_mail.status = "error"
					   		sent_mail.response_message = "Postage API failure - #{result["response"]["message"]}"
						end

					rescue => e
						# puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
						# puts "~~ Postage send failure: #{e.class} - #{e.message}"
						# puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
						sent_mail.sent = false
						sent_mail.status = "error"
					    sent_mail.response_message = "Postage send failure: #{e.class} - #{e.message}"
					end

				when "socketlabs"
					###############################################################
					## From http://www.socketlabs.com/api-reference/injection-api/
					###############################################################
					begin
						#Socketlabs.api_user='#{api_name}'
						#Socketlabs.api_password='#{api_key}'

						#SocketLabs.url="https://api.socketlabs.com/v1/email/send"
						##SocketLabs.user="#{api_name}"
						##SocketLabs.pw="#{api_key}"

						## You can instantiate the EmailMessage object and work with its properties and methods
						#msg = SocketLabs::EmailMessage.new
						#msg.from = "#{sender_email}"
						#msg.to = "#{recipient}"
						#msg.subject = "#{subject}"

						## You can specify a text and/or HTML body.
						#msg.htmlBody = "#{content}"

						## You can specify custom headers
						#msg.headers = {:X-MyHeader=>'Value', :List-Unsubscribe=>'Value'}

						#msg.send

						result_args = Hash.new
						messages = Hash.new
						message_to = Hash.new
						message_from = Hash.new
						reply_to = Hash.new

						message_to["EmailAddress"] = "#{recipient}"
						message_from["EmailAddress"] = "#{sender}"

						reply_to["EmailAddress"] = "#{sender}"
						reply_to["FriendlyName"] = "#{sender}"

						messages["To"] = message_to
						messages["From"] = message_from
						messages["Subject"] = "#{subject}"
						#messages["TextBody"] = nil
						messages["HtmlBody"] = "#{body}"
						messages["ReplyTo"] = reply_to
						messages["MailingId"] = nil
        				messages["MessageId"] = nil
        				messages["MergeData"] = nil
        				messages["CustomHeaders"] = nil

						result_args["ServerID"] = "#{api_name}"
						result_args["ApiKey"] = "#{api_key}"
						result_args["messages"] = messages
						result_args["Cc"] =  nil
        				result_args["Bcc"] =  nil
        				result_args["ReplyTo"] = nil
				        result_args["Charset"] = nil
				        result_args["Attachments"] = nil

						result = HTTParty.post("https://inject.socketlabs.com/api/v1/email",
				    	:body => result_args.to_json,
				    	:headers => { 'Content-Type' => 'application/json' } )

						if result["ErrorCode"] == "Success"
							sent_mail.sent = true
					    	sent_mail.response_message = "Message sent via Socket Labs"
						else
							sent_mail.sent = false
					   		sent_mail.response_message = "Postage API failure - #{result["response"]["message"]}"
						end

						sent_mail.sent = true
						sent_mail.response_message = "Message sent via SocketLabs."
						rescue => e
						sent_mail.sent = false
						sent_mail.response_message = "Message could not be sent. #{e.message}"
					end

				when "elastic_email"
					begin
						result = HTTParty.post("https://api.elasticemail.com/mailer/send",
						#result = HTTParty.post("https://#{api_key}:#{api_name}@api.elasticemail.com/mailer/send",
					    	:body => { "username" => "#{api_name}",
							  "api_key" => "#{api_key}",
							  "from" => "#{sender}",
							  "from_name" => "#{sender_name}",
							  "to" => "#{recipient}",
							  "subject" => "#{subject}",
							  "body_html" => "#{content}",
							  "reply_to" => "#{sender}",
							  "reply_to_name" => "#{sender_name}"
					    	})

						unless result == /#^Error:/
							sent_mail.sent = true
					    	sent_mail.response_message = "Message sent via Elastic Email"
							sent_mail.ext_id = result
						else
							sent_mail.sent = false
					   		sent_mail.response_message = "#{result["response"]["message"]}"
						end

						sent_mail.sent = true
						sent_mail.response_message = "Message sent via Elastic Email."
						sent_mail.ext_id = result
					rescue => e
						sent_mail.sent = false
						sent_mail.response_message = "Message could not be sent. #{e.message}"
					end

					###############################################################
					## From http://elasticemail.com/api-documentation/send
					###############################################################

					#Send command POST to https://api.elasticemail.com/mailer/send with the following form values:

					#username=your account email address
					#api_key=your api key
					#from=from email address
					#from_name=display name for from email address
					#to=semi colon separated list of email recipients (each email is treated separately, like a BCC)
					#subject=email subject
					#body_html=html email body [optional]
					#body_text=text email body [optional]
					#reply_to=email address to reply to [optional]
					#reply_to_name=display name of the reply to address [optional]
					#channel=an id field (max 60 chars) that can be used for reporting [optional - will default to from address]
					#charset=text value of encoding for example: iso-8859-1, windows-1251, utf-8, us-ascii, windows-1250 and more…
					#encodingtype=0 for None, 1 for Raw7Bit, 2 for Raw8Bit, 3 for QuotedPrintable, 4 for Base64 (Default), 5 for Uue  note that you can also provide the text version such as "Raw7Bit" for value 1.  NOTE: Base64 or QuotedPrintable is recommended if you are validating your domain(s) with DKIM.

					#Optional Templates and contact lists:
					#template=the name of an email template you have created in your account. If you send a template [optional]
					#merge_firstname,merge_lastname= if sending to a template you can send merge_ fields to merge data with the template. Template fields are entered with {firstname}, {lastname} etc. [optional]

					#lists=the name of a contact list you would like to send to. Separate multiple contact lists by commas. [optional]
					#Optional sender header. If you are sending on behalf of many clients with different from addresses use:

					#sender=email address of the sender [optional]
					#sender_name=display name sender [optional]
					#Optional scheduling. If you would like to schedule your email to be delivered in the future (drip campaigns etc):

					#time_offset_minutes=number of minutes in the future this email should be sent [optional]
					#Optional Custom Headers.  If you would like to add custom headers to your email you can do so by defining parameter's header1, header2, header3, etc and providing it a custom header name and header value.  Note: a space is required after the colon before the custom header value.
					#header1=customheader1: header-value1
					#header2=customheader2: header-value2
					#header3=customheader3: header-value3
				#when "postmark"
			end

			sent_mail.save
		else
			send_results["response_message"] = "No config parameters provided. Unable to proceed."
			return false
		end
		return true
	end

	def replace_links_in_message(msg, tracking_code)
		message = Nokogiri::HTML.fragment(msg)

		message.css('a[href]').each do |link|
		#################################################
		## TODO: Change to SSL when implemented.
		#################################################
		  link["href"] = "http://www.klickrr.net/lnks/#{tracking_code}?url=#{link['href']}"
		end

		return message.to_html
	end
end
