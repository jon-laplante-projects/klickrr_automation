# == Schema Information
#
# Table name: service_fields
#
#  id            :integer          not null, primary key
#  service_id    :integer          not null
#  label         :string(255)
#  field_type    :string(255)
#  required      :boolean
#  field_options :text
#  description   :text
#  min           :integer
#  max           :integer
#  created_at    :datetime
#  updated_at    :datetime
#  name          :string(255)
#

class ServiceField < ActiveRecord::Base
	belongs_to :service
end
