# == Schema Information
#
# Table name: form_templates
#
#  id            :integer          not null, primary key
#  name          :string
#  label         :string
#  markup        :text
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  preview_image :string
#

class FormTemplate < ActiveRecord::Base
end
