# == Schema Information
#
# Table name: followup_scopes_triggers
#
#  id                  :integer          not null, primary key
#  followup_scope_id   :integer
#  followup_trigger_id :integer
#

class FollowupScopesTrigger < ActiveRecord::Base
  belongs_to :followup_scope
  belongs_to :followup_trigger
end
