# == Schema Information
#
# Table name: list_field_groups
#
#  id           :integer          not null, primary key
#  list_id      :integer
#  label        :string(255)
#  created_at   :datetime
#  updated_at   :datetime
#  form_style   :text
#  form_code    :string
#  redirect_url :string
#

class ListFieldGroup < ActiveRecord::Base
	has_many :list_fields#, dependent: :destroy
	belongs_to :list
end
