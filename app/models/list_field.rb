# == Schema Information
#
# Table name: list_fields
#
#  id                  :integer          not null, primary key
#  label               :string(255)
#  field_type          :string(255)
#  required            :boolean
#  field_options       :text
#  description         :text
#  include_other       :boolean
#  include_blank       :boolean
#  integer_only        :boolean
#  min                 :integer
#  max                 :integer
#  created_at          :datetime
#  updated_at          :datetime
#  cid                 :string(255)
#  list_id             :integer
#  list_field_group_id :integer
#  is_email            :boolean
#  name                :string(255)
#

class ListField < ActiveRecord::Base
	before_save :gen_list_col_name

	belongs_to :list_field_group
	belongs_to :list
	has_many :list_values#, dependent: :destroy

	def gen_list_col_name(sfx=nil)
	  	if self.name.blank?
		  	label_arr = self.label.split("_")

		  	# Lowercase, strip everything but alphanumeric, replace spaces with underscores.
		  	col_name = self.label.gsub(/[^0-9a-z]/i, '').gsub(" ", "_").downcase

		  	# Test if there is a numbered suffix already...
			if !/\A\d+\z/.match(label_arr.last)
				#Is not a positive number
				suffix = sfx.to_s
			else
				#Is all good ..continue
				suffix = sfx.to_i || label_arr.last.to_i + 1
				col_name = col_name.concat("_#{sfx}")
			end

		  	col = ListField.where(list_id: list_id, name: "#{col_name}").count

		  	if col > 1
		  		if suffix.nil?
		  			suffix = 1
		  		end

		  		gen_list_col_name(suffix)
		  	else
		  		self.name = col_name
		  		#self.save
		  		return
		  	end
	  	end
  end
end
