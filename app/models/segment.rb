class Segment < ActiveRecord::Base
  belongs_to :segment_operation, foreign_key: :segment_operator_id, class_name: 'SegmentOperation'
  belongs_to :message, polymorphic: true
  belongs_to :list_field, foreign_key: :field_id, class_name: 'ListField'
  belongs_to :list

  def get_emails
    set_ids = ListField.where(label: self.list_field.label, list_id: list.id).last.list_values.where(self.sql).pluck(:set_id) 
    list_field = ListField.where(label: 'email', list_id: list.id)
    list_field = ListField.where('lower(label) = "email" AND list_id = ?',list.id) if list_field.blank?
    list_field.first.list_values.where(set_id: set_ids).pluck(:field_value)
  end

  def set_sql
    self.sql = self.segment_operation.expression.gsub('{{value}}', self.condition_value)
    save
  end

end
