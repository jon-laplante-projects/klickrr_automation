class Service < ActiveRecord::Base
	belongs_to :service_type
	has_many :service_fields, dependent: :destroy
	has_many :services_users, dependent: :destroy

end