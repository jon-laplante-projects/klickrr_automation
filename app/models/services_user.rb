class ServicesUser < ActiveRecord::Base
	include ApplicationHelper
	after_create :activate_webhooks
	belongs_to :service
	attr_accessor :api_password

	WEBHOOKS_SERVER = "http://104.131.80.10/webhooks/"

	def activate_webhooks
		usage = read_vars(self.usage)

		case self.service.name
			when 'personal'
				# Using internal "lite" tracking - no webhooks to set up.

			when 'sendgrid'
				# Sendgrid
				activate_string = "api_user=#{usage["api_name"]}&api_key=#{self.api_password}&name=eventnotify&processed=0&dropped=1&open=1&deferred=0&delivered=1&bounce=1&click=1&unsubscribe=1&spamreport=1&url=#{WEBHOOKS_SERVER}sendgrid&version=3"

				#puts "Activate Sendgrid Webhooks: #{activate_string}"
				 result = HTTParty.post("https://api.sendgrid.com/api/filter.setup.json",
				     	:body => activate_string,
				     	:headers => { 'Content-Type' => 'application/json' } )

			when 'mandrill'
				# Mandrill
				activate_hash = { key: "#{usage["api_key"]}", url: "#{WEBHOOKS_SERVER}mandrill", description: "Klickrr - Mandrill Webhook", events: ["send","hard_bounce","soft_bounce","open","click","spam","unsub","reject"] }
				activate_string = activate_hash.to_json

				 result = HTTParty.post("https://mandrillapp.com/api/1.0/webhooks/add.json",
				     	:body => activate_string,
				     	:headers => { 'Content-Type' => 'application/json' } )

			when 'mailgun'
				# MailGun

				# For MailGun, we will have to create a different webhook for every event we intend to track.
				begin
					for x in 1..7
					  case x
					  	when 1
					  		id = "bounce"
					  		url = "#{WEBHOOKS_SERVER}mailgun&event_name=bounce"
					  	when 2
					  		id = "deliver"
					  		url = "#{WEBHOOKS_SERVER}webhooks/mailgun&event_name=deliver"
					  	when 3
							id = "drop"
							url = "#{WEBHOOKS_SERVER}mailgun&event_name=drop"
					  	when 4
							id = "spam"
							url = "#{WEBHOOKS_SERVER}mailgun&event_name=spam"
					  	when 5
							id = "unsubscribe"
							url = "#{WEBHOOKS_SERVER}mailgun&event_name=unsubscribe"
					  	when 6
							id = "click"
							url = "#{WEBHOOKS_SERVER}mailgun&event_name=click"
					  	when 7
							id = "open"
							url = "#{WEBHOOKS_SERVER}mailgun&event_name=open"
					  end

	          if HTTParty.get("https://api:#{usage["api_key"]}@api.mailgun.net/v3/domains/#{usage["domain"]}/webhooks/#{id}").parsed_response["message"] == "Webhook not found"
						#unless RestClient.get("https://api:#{usage["api_key"]}@api.mailgun.net/v3/domains/#{usage["domain"]}/webhooks/#{id}").present?
							result = HTTParty.post("https://api:#{usage["api_key"]}@api.mailgun.net/v3/domains/#{usage["domain"]}/webhooks", body: {id: "#{id}", url: "#{url}"})
							puts "MailGun WebHooks result: #{result}"
							# result = RestClient.post("https://api:#{usage["api_key"]}@api.mailgun.net/v3/domains/#{usage["domain"]}/webhooks",
	             #         :id => "#{id}",
	             #         :url => "#{url}")
	          else
							result = HTTParty.put("https://api:#{usage["api_key"]}@api.mailgun.net/v3/domains/#{usage["domain"]}/webhooks/#{id}", body: { url: "#{url}" })
							puts "MailGun WebHooks result: #{result}"
							# result = RestClient.put("https://api:#{usage["api_key"]}@api.mailgun.net/v3/domains/#{usage["domain"]}/webhooks/#{id}",
	             #         :url => "#{url}")
	          end
					end
				rescue => e
					raise e
				end

			when 'mailjet'
				# MailJet
				usr = User.find(self.user_id)

				unless usage["from_email"].blank?
					email_hash = { Name: "#{usr.first_name} #{usr.last_name}", Email: "#{usage["from_email"]}" }
				else
					email_hash = { Name: "#{usr.first_name} #{usr.last_name}", Email: "#{usr.email}" }
				end
				email = email_hash.to_json

				begin
					result = HTTParty.post("https://#{usage["api_key"]}:#{usage["api_name"]}@api.mailjet.com/v3/REST/sender",
    				    	:body => email,
    				    	:headers => { "Content-Type" => "application/json" } )

					begin
						for event in 1..7 do
							case event
								when 1
									evt = "open"

								when 2
									evt = "click"

								when 3
									evt = "bounce"

								when 4
									evt = "spam"

								when 5
									evt = "blocked"

								when 6
									evt = "unsub"

								when 7
									evt = "sent"
							end

							mailjet_api_rec = HTTParty.get("https://#{usage["api_key"]}:#{usage["api_name"]}@api.mailjet.com/v3/REST/apikey")

							webhook = HTTParty.post("https://#{usage["api_key"]}:#{usage["api_name"]}@api.mailjet.com/v3/REST/eventcallbackurl",
								:body => { APIKeyID: "#{mailjet_api_rec["Data"][0]["ID"]}", EventType: "#{evt}",  Url: "#{WEBHOOKS_SERVER}mailjet"}.to_json,
								:headers => {"Content-Type" => "application/json"} )

							# puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
							# puts "Conn string: https://#{usage["api_key"]}:#{usage["api_name"]}@api.mailjet.com/v3/REST/eventcallbackurl"
							# puts "~~ Response: #{webhook.inspect}"
							# puts "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
						end

					rescue Exception => err
						puts "Error: #{err.inspect}"
					end
				rescue Exception => e
					puts "Error: #{e.inspect}"
				end

			when 'ses'
				# Amazon SES
				# Using internal "lite" tracking - no webhooks to set up.

			when 'postage'
				# Postage
				# Using internal "lite" tracking - no webhooks to set up.

			when 'socketlabs'
				# Socket Labs

			when 'elastic_email'
				# Elastic Email

			when 'twilio'
				# Twilio
				
			when 'sparkpost'
				# SparkPost
				body = {
					name: 'klickrr webhook',
					target: "#{WEBHOOKS_SERVER}sparkpost",
					events: [
						"delivery",
						"injection",
						"open",
						"click",
						"bounce",
						"spam_complaint"
					]
				}

				result = HTTParty.post("https://api.sparkpost.com/api/v1/webhooks", body: body.to_json, headers: { "Authorization" => usage['api_key'], 'Content-Type' => 'application/json'  })
		end
	end
end
