class FollowupType < ActiveRecord::Base
  has_many :followups, foreign_key: :type_id, class_name: "Followup"
end