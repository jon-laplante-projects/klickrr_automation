
# == Schema Information
#
# Table name: sent_mails
#
#  id               :integer          not null, primary key
#  user_id          :integer
#  service_id       :integer
#  message_id       :integer
#  message_type     :string(255)
#  email            :string(255)
#  clicks           :text
#  created_at       :datetime
#  updated_at       :datetime
#  tracking_code    :string(255)
#  contact_id       :integer
#  sent             :boolean
#  response_message :text
#  opened           :datetime
#  dropped          :datetime
#  delivered        :datetime
#  bounced          :datetime
#  unsubscribed     :datetime
#  spam             :datetime
#  blocked          :datetime
#  ext_id           :string
#  status           :string
#  dirty            :boolean
#

class SentMail < ActiveRecord::Base
  include ApplicationHelper
	belongs_to :list
	belongs_to :user
	has_many :followups, through: :user

  # GEOIP = GeoIP.new(Rails.root.join('lib/GeoLiteCity.dat'))

	def generate_token(column, length = 64)
	  begin
	    self[column] = SecureRandom.urlsafe_base64 length
	  end while SentMail.exists?(column => self[column])
	end

  def parse_clicks
    read_vars(self.clicks)
  end  

	def save_click(url, ip_addr)
    click_arr = []
    click = { url: url, clicked_at: Time.now.to_s, ip_addr: ip_addr }

    if self.clicks.present?
      read_vars(self.clicks).each do |c|
        click_arr << c
      end
    end

    click_arr << click
    self.clicks = click_arr.to_s
    save
  end

  def save_open(ip_addr)
    open_arr = []
    open_event = { opened_at: Time.now.to_s, ip_addr: ip_addr }

    if self.opened.present?
      read_vars(self.opened).each do |c|
        open_arr << c
      end
    end

    open_arr << open_event
    self.opened = open_arr.to_s
    save
  end

  def save_unsubscribe(ip_addr)
    unsubscribe_arr = []
    unsubscribe_event = { unsubscribed_at: Time.now.to_s, ip_addr: ip_addr }

    if self.unsubscribed.present?
      eval(self.unsubscribed).each do |c|
        unsubscribe_arr << c
      end
    end

    unsubscribe_arr << unsubscribe_event
    self.unsubscribed = unsubscribe_arr.to_s
    save
  end

  def states_by_key(collection, key)
    if self[collection].present?
      counts = Hash.new(0)
      read_vars(self[collection]).map { |var| var[key] }.each { |country| counts[country] += 1 }
      counts
    end  
  end

  def states_of_key_by_key(collection, main_key, state_key)
    if self[collection].present?
      vars = read_vars(self[collection])
      links = {}
      vars.group_by { |var| var[main_key] }.each { |k, v| links[k] = v.group_by { |var_2| var_2[state_key] } }
      links
    end  
  end

  def count_of_key_by_key(collection, main_key, state_key)
    if self[collection].present?
      states = states_of_key_by_key(collection, main_key, state_key)
      counts = {}
      states.each do |key, value|
        counts[key] = {}
        value.each { |k, v| counts[key][k] = v.count } 
      end
      counts 
    end  
  end

  def count_events(collection)
    if self[collection].present?
      read_vars(self[collection]).count
    else
      0
    end  
  end

  def day_count(collection, time_field)
    if self[collection].present?
      result = {}
      read_vars(self[collection]).group_by { |item| (DateTime.parse(item[time_field]).to_date - self.created_at.to_date).to_i }.each { |k, v| result[k] = v.count }
      result
    end  
  end

  def check_spam
    {(self.spam.to_date - self.created_at.to_date).to_i => 1} if self.spam.present?
  end

  private

  def search_location(ip)
    [:opened, :clicks, :unsubscribed].each do |collection|
      if SentMail.last[collection].present?
        eval(SentMail.last[collection]).each do |item|
          location = item.slice(:city, :region, :country, :timezone)
          return location if item[:ip_addr] == ip && location.present? && location.values.all? { |x| !x.nil? }
        end
      end
    end
    nil
  end

  def get_location(ip)
    if ip.present?
      result = GEOIP.city(ip)
      if result.present?
        {
            city: result.city_name,
            region: result.real_region_name,
            country: result.country_name,
            timezone: result.timezone
        }
      end
    end
  end

  def add_to_collection(collection, item)
    item.merge!({city: nil, region: nil, country: nil, timezone: nil})
    if item[:ip_addr].present?
      location = search_location(item[:ip_addr])
      location ||= get_location(item[:ip_addr])
      item.merge!(location) if location.present?
    end

    arr = []
    arr = eval(self[collection]) if self[collection].present?
    arr << item
    update_attribute(collection, arr.to_s)
  end
end
