# == Schema Information
#
# Table name: followup_scopes
#
#  id    :integer          not null, primary key
#  label :string
#  name  :string
#

class FollowupScope < ActiveRecord::Base
  has_many :followup_scopes_triggers
  has_many :followup_triggers, through: :followup_scopes_triggers
end
