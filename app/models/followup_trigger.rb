# == Schema Information
#
# Table name: followup_triggers
#
#  id              :integer          not null, primary key
#  descr           :text
#  code            :text
#  variables       :text
#  negative        :boolean
#  scope_id        :integer
#  trigger_type_id :integer
#

class FollowupTrigger < ActiveRecord::Base
  has_many :followup_scopes_triggers
  has_many :followup_scopes, through: :followup_scopes_triggers
end
