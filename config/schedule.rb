# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
set :output, "/webapps/klickrr_automation/log/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
every 1.minute do
    command "cd /webapps/klickrr_automation && RAILS_ENV=production bundle exec rake mail_tasks:message_sender"
    # command "cd /webapps/klickrr_automation && RAILS_ENV=production /usr/local/rvm/wrappers/ruby-2.2.1 -S bundle exec rake mail_tasks:message_sender"
    # command "rvm use ruby-2.2.1 && cd /webapps/klickrr_automation && RAILS_ENV=production /usr/local/rvm/wrappers/ruby-2.2.1 -S bundle exec rake mail_tasks:message_sender"
    # rake "mail_tasks:message_sender"


    # runner "Message.new(message: 'Hey, it worked!').save"
end

# Check if we need to move something into the Queue.
every 1.minute do
# every 6.hours do
    command "cd /webapps/klickrr_automation && RAILS_ENV=production bundle exec rake check_followups_triggers"
    # command "cd /webapps/klickrr_automation && RAILS_ENV=production /usr/local/rvm/wrappers/ruby-2.2.1 -S bundle exec rake check_followups_triggers"
end

# Check the message queue and see if we need to send anything.
# every 4.hours do
every 1.minute do
    command "cd /webapps/klickrr_automation && RAILS_ENV=production bundle exec rake delivery_of_queued_messages"
end
